/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BoundedRangeModel;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author Marco Gutama
 */
public class ventanaPrincipal extends javax.swing.JFrame {

    public static String cusuario;
    public static int czona;
    UsuarioLab usuario;
    private Software software;
    public Equipo equipo;
    private Oficina oficina;
    public GLPI glpi;
    public Material material;
    public Departamento departamento;
    public ConsultasSoftware conSoft;
    public ConsultasEquipos conEquipos;
    public ConsultasOficinas conOficinas;
    public ConsultasGLPI conGLPI;
    public ConsultasMateriales conMateriales;
    public ConsultasDepartamentos conDepartamento;
    public ConsultasTipoHardware conTipoHardware;

    JComboBox<Oficina> comboBoxOficina;
    JComboBox<Departamento> comboBoxDepartamento;
    JComboBox<String> comboBoxTipoHardware;
    JComboBox<String> comboBoxMarcaHardware;

    DefaultTableModel modeloTabEquipos;
    DefaultTableModel modeloTabOficinas;
    DefaultTableModel modeloTabEquiposOficina;
    DefaultTableModel modeloTabSoftware;
    DefaultTableModel modeloTabEquiposSoftware;
    DefaultTableModel modeloTabGlpi;
    DefaultTableModel modeloTabMateriales;
    DefaultTableModel modeloTabMovimientos;
    DefaultTableModel modeloTabDepartamentos;

    List<ControlTabla> registroTablaEquipos;
    List<ControlTabla> registroTablaOficinas;
    List<ControlTabla> registroTablaDepartamentos;
    List<ControlTabla> registroTablaMateriales;
    List<ControlTabla> registroTablaSoftware;

    /**
     * Creates new form ventanaPrincipal
     */
    public ventanaPrincipal() {
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        software = new Software();
        conSoft = new ConsultasSoftware();
        equipo = new Equipo();
        conEquipos = new ConsultasEquipos();
        oficina = new Oficina();
        conOficinas = new ConsultasOficinas();
        glpi = new GLPI();
        conGLPI = new ConsultasGLPI();
        material = new Material();
        conMateriales = new ConsultasMateriales();
        departamento = new Departamento();
        conDepartamento = new ConsultasDepartamentos();
        conTipoHardware = new ConsultasTipoHardware();

        modeloTabEquipos = (DefaultTableModel) tabListadoEquipos.getModel();
        modeloTabEquiposSoftware = (DefaultTableModel) tabListadoEquiposSoftware.getModel();
        modeloTabGlpi = (DefaultTableModel) tabListadoGLPI.getModel();
        modeloTabMateriales = (DefaultTableModel) tabListadoMateriales.getModel();
        modeloTabMovimientos = (DefaultTableModel) tabListadoMovimientos.getModel();

        registroTablaEquipos = new ArrayList<>();
        registroTablaOficinas = new ArrayList<>();
        registroTablaDepartamentos = new ArrayList<>();
        registroTablaMateriales = new ArrayList<>();
        registroTablaSoftware = new ArrayList<>();

        TableColumn columnaEstado = tabListadoEquipos.getColumnModel().getColumn(13);
        JComboBox comboBoxEstado = new JComboBox();
        comboBoxEstado.addItem("");
        comboBoxEstado.addItem("A");
        comboBoxEstado.addItem("B");
        comboBoxEstado.addItem("D");
        comboBoxEstado.addItem("R");
        columnaEstado.setCellEditor(new DefaultCellEditor(comboBoxEstado));

        //////////
        //Proceso para ocultar las tabs, excepto login
        limpiarInterfaz();
        ////////////
        tabBuscarEquipos.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        tabListadoEquipos.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        tabBuscarEquiposSoftware.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        tabListadoEquiposSoftware.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        tabBuscarMateriales.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        tabListadoMateriales.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        tabBuscarGLPI.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        tabListadoGLPI.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        ///////////// Para evitar que con ENTER vaya a la fila inferior
        tabListadoEquipos.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "selectNextColumnCell");
        tabListadoEquiposSoftware.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "selectNextColumnCell");
        tabListadoMateriales.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "selectNextColumnCell");
        tabListadoGLPI.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "selectNextColumnCell");
        //////////
        jScrollPane2.getColumnHeader().setVisible(false);//Quita el columnheader de la tabla buscar

        Action action = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                TableCellListener tcl = (TableCellListener) e.getSource();
                System.out.println("Row   : " + tcl.getRow());
                System.out.println("Column: " + tcl.getColumn());
                System.out.println("Old   : " + tcl.getOldValue());
                System.out.println("New   : " + tcl.getNewValue());

                controlCambiosTabla(tcl, registroTablaEquipos);
            }
        };
        TableCellListener tcl = new TableCellListener(tabListadoEquipos, action);

        Action actionTabMateriales = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                TableCellListener tcl = (TableCellListener) e.getSource();
                System.out.println("Row   : " + tcl.getRow());
                System.out.println("Column: " + tcl.getColumn());
                System.out.println("Old   : " + tcl.getOldValue());
                System.out.println("New   : " + tcl.getNewValue());

                controlCambiosTabla(tcl, registroTablaMateriales);
            }
        };
        TableCellListener tclMateriales = new TableCellListener(tabListadoMateriales, actionTabMateriales);

        //Moficar ancho tabla buscar de acuerdo a ancho tabla equipos
        for (int m = 0; m < 17; m++) {
            tabBuscarEquipos.getColumnModel().getColumn(m).setPreferredWidth(tabListadoEquipos.getTableHeader().getColumnModel().getColumn(m).getPreferredWidth());
        }
        tabListadoEquipos.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
            public void columnMarginChanged(ChangeEvent e) {
                if (tabListadoEquipos.getTableHeader().getResizingColumn() != null) {
                    for (int m = 0; m < 17; m++) {
                        tabBuscarEquipos.getColumnModel().getColumn(m).setPreferredWidth(tabListadoEquipos.getTableHeader().getColumnModel().getColumn(m).getPreferredWidth());
                    }

                }
            }

            public void columnAdded(TableColumnModelEvent e) {
            }

            public void columnRemoved(TableColumnModelEvent e) {
            }

            public void columnMoved(TableColumnModelEvent e) {
            }

            public void columnSelectionChanged(ListSelectionEvent e) {
            }
        });
        BoundedRangeModel model = jScrollPane2.getHorizontalScrollBar().getModel();
        jScrollPane1.getHorizontalScrollBar().setModel(model);
        jScrollPane2.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        //jScrollPane2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        //Moficar ancho tabla buscar de acuerdo a ancho tabla GLPIs
        for (int m = 0; m < 6; m++) {
            tabBuscarGLPI.getColumnModel().getColumn(m).setPreferredWidth(tabListadoGLPI.getTableHeader().getColumnModel().getColumn(m + 2).getPreferredWidth());
        }
        tabListadoGLPI.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
            public void columnMarginChanged(ChangeEvent e) {
                if (tabListadoGLPI.getTableHeader().getResizingColumn() != null) {
                    for (int m = 0; m < 6; m++) {
                        tabBuscarGLPI.getColumnModel().getColumn(m).setPreferredWidth(tabListadoGLPI.getTableHeader().getColumnModel().getColumn(m + 2).getPreferredWidth());
                    }

                }
            }

            public void columnAdded(TableColumnModelEvent e) {
            }

            public void columnRemoved(TableColumnModelEvent e) {
            }

            public void columnMoved(TableColumnModelEvent e) {
            }

            public void columnSelectionChanged(ListSelectionEvent e) {
            }
        });

        //Moficar ancho tabla buscar de acuerdo a ancho tabla Software
        for (int m = 0; m < 9; m++) {
            tabBuscarEquiposSoftware.getColumnModel().getColumn(m).setPreferredWidth(tabListadoEquiposSoftware.getTableHeader().getColumnModel().getColumn(m).getPreferredWidth());
        }
        tabListadoEquiposSoftware.getColumnModel().addColumnModelListener(new TableColumnModelListener() {
            public void columnMarginChanged(ChangeEvent e) {
                if (tabListadoEquiposSoftware.getTableHeader().getResizingColumn() != null) {
                    for (int m = 0; m < 9; m++) {
                        tabBuscarEquiposSoftware.getColumnModel().getColumn(m).setPreferredWidth(tabListadoEquiposSoftware.getTableHeader().getColumnModel().getColumn(m).getPreferredWidth());
                    }

                }
            }

            public void columnAdded(TableColumnModelEvent e) {
            }

            public void columnRemoved(TableColumnModelEvent e) {
            }

            public void columnMoved(TableColumnModelEvent e) {
            }

            public void columnSelectionChanged(ListSelectionEvent e) {
            }
        });

    }

    public void controlCambiosTabla(TableCellListener tcl, List<ControlTabla> registroTablaEquipos) {
        ControlTabla controlTabla = new ControlTabla(tcl.getRow(), tcl.getColumn(), "" + tcl.getOldValue(), "" + tcl.getNewValue());
        registroTablaEquipos.add(controlTabla);
    }

    public void limpiarInterfaz() {
        jTabbedPane1.removeAll();
        jTabbedPane1.addTab("Login", jpLogin);
        this.getRootPane().setDefaultButton(btIngresar);
        /////
        menuUsuariosLab.setVisible(false);
        menuRespaldarDB.setVisible(false);
        menuRestaurarDB.setVisible(false);
        btMenu.setVisible(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        menuOficinas = new javax.swing.JMenuItem();
        menuDepartamentos = new javax.swing.JMenuItem();
        menuSoftware = new javax.swing.JMenuItem();
        menuUsuariosPC = new javax.swing.JMenuItem();
        menuUsuariosLab = new javax.swing.JMenuItem();
        menuReportes = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuRespaldarDB = new javax.swing.JMenuItem();
        menuRestaurarDB = new javax.swing.JMenuItem();
        menuAcercaDe = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuCerrarSesion = new javax.swing.JMenuItem();
        menuSalir = new javax.swing.JMenuItem();
        jftxtNumero = new javax.swing.JFormattedTextField();
        jftxtCodAct = new javax.swing.JFormattedTextField();
        jftxtRAM = new javax.swing.JFormattedTextField();
        txtMsg = new javax.swing.JLabel();
        txtLabelUsuario = new javax.swing.JLabel();
        panelGlobal = new javax.swing.JPanel();
        btMenu = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jpHardware = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabListadoEquipos = new javax.swing.JTable();
        btGuardarEquipo = new javax.swing.JButton();
        btEliminarEquipo = new javax.swing.JButton();
        btNuevoEquipo = new javax.swing.JButton();
        btBuscarEquipos = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabBuscarEquipos = new javax.swing.JTable();
        btAgregarGLPI = new javax.swing.JButton();
        btEquipoUserSoft = new javax.swing.JButton();
        jpEquipoSoftware = new javax.swing.JPanel();
        btBuscarSoftwareEquipos = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        tabListadoEquiposSoftware = new javax.swing.JTable();
        jScrollPane9 = new javax.swing.JScrollPane();
        tabBuscarEquiposSoftware = new javax.swing.JTable();
        jpMateriales = new javax.swing.JPanel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane11 = new javax.swing.JScrollPane();
        tabListadoMateriales = new javax.swing.JTable();
        btNuevoMaterial = new javax.swing.JButton();
        btRegistrar = new javax.swing.JButton();
        btBuscarMaterial = new javax.swing.JButton();
        jScrollPane15 = new javax.swing.JScrollPane();
        tabBuscarMateriales = new javax.swing.JTable();
        btEntradaMaterial = new javax.swing.JButton();
        btSalidaMaterial = new javax.swing.JButton();
        btEliminarMaterial = new javax.swing.JButton();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane14 = new javax.swing.JScrollPane();
        tabListadoMovimientos = new javax.swing.JTable();
        btBuscarMovimiento = new javax.swing.JButton();
        jdcFechaMovimiento = new com.toedter.calendar.JDateChooser();
        txtDescMaterial = new javax.swing.JTextField();
        jpGLPI = new javax.swing.JPanel();
        jScrollPane12 = new javax.swing.JScrollPane();
        tabBuscarGLPI = new javax.swing.JTable();
        btBuscarGlpi = new javax.swing.JButton();
        btEliminarGLPI = new javax.swing.JButton();
        jScrollPane13 = new javax.swing.JScrollPane();
        tabListadoGLPI = new javax.swing.JTable();
        btModificarGLPI = new javax.swing.JButton();
        jpLogin = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtContraseña = new javax.swing.JPasswordField();
        txtUsuario = new javax.swing.JTextField();
        btIngresar = new javax.swing.JButton();

        menuOficinas.setText("Oficinas/Nodos");
        menuOficinas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuOficinasActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuOficinas);

        menuDepartamentos.setText("Departamentos");
        menuDepartamentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDepartamentosActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuDepartamentos);

        menuSoftware.setText("Software");
        menuSoftware.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSoftwareActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuSoftware);

        menuUsuariosPC.setText("Usuarios");
        menuUsuariosPC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsuariosPCActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuUsuariosPC);

        menuUsuariosLab.setText("Usuarios Lab");
        menuUsuariosLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuUsuariosLabActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuUsuariosLab);

        menuReportes.setText("Reportes");
        menuReportes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuReportesActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuReportes);
        jPopupMenu1.add(jSeparator1);

        menuRespaldarDB.setText("Respaldar DB");
        menuRespaldarDB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRespaldarDBActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuRespaldarDB);

        menuRestaurarDB.setText("Restaurar DB");
        menuRestaurarDB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRestaurarDBActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuRestaurarDB);
        menuRestaurarDB.setEnabled(false);

        menuAcercaDe.setText("Acerca De");
        menuAcercaDe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAcercaDeActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuAcercaDe);
        jPopupMenu1.add(jSeparator2);

        menuCerrarSesion.setText("Cerrar sesión");
        menuCerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCerrarSesionActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuCerrarSesion);

        menuSalir.setText("Salir");
        menuSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSalirActionPerformed(evt);
            }
        });
        jPopupMenu1.add(menuSalir);

        jftxtNumero.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        try {
            jftxtNumero.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jftxtCodAct.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        try {
            jftxtCodAct.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#.#.##.##.###.####.###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jftxtRAM.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        try {
            jftxtRAM.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("INVENTARIO EQUIPOS JARDÍN AZUAYO - 1.8");

        txtMsg.setText(" ");

        txtLabelUsuario.setText("              ");

        panelGlobal.setPreferredSize(new java.awt.Dimension(1270, 450));
        panelGlobal.setLayout(new javax.swing.OverlayLayout(panelGlobal));

        btMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menuBar.png"))); // NOI18N
        btMenu.setAlignmentX(1.0F);
        btMenu.setAlignmentY(0.0F);
        btMenu.setContentAreaFilled(false);
        btMenu.setIconTextGap(0);
        btMenu.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btMenu.setMaximumSize(new java.awt.Dimension(20, 20));
        btMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btMenuActionPerformed(evt);
            }
        });
        panelGlobal.add(btMenu);

        jTabbedPane1.setAlignmentX(1.0F);
        jTabbedPane1.setAlignmentY(0.0F);
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(1270, 450));

        tabListadoEquipos.setAutoCreateRowSorter(true);
        tabListadoEquipos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "# Activo", "Tipo", "Marca", "Modelo", "Serial", "Procesador", "RAM (GB)", "HDD (GB)", "Nombre", "Usuario", "MAC", "IP", "Estado", "Observacion", "Id Oficina", "Oficina/Nodo", "Departamento" , "Ubicacion", "Fecha"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true, true, true, true, true, true, true, false, true, true, true, true,true, true, true, true, true
            };
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tabListadoEquipos.getTableHeader().setReorderingAllowed(false);
        tabListadoEquipos.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tabListadoEquipos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        //tabListadoEquipos.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(jftxtCodAct));
        tabListadoEquipos.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent evt) {
                //enable button - put it in an EDT to be safe though
                tabListadoEquiposListaSeleccionada(evt);
            }
        });
        jScrollPane1.setViewportView(tabListadoEquipos);
        if (tabListadoEquipos.getColumnModel().getColumnCount() > 0) {
            tabListadoEquipos.getColumnModel().getColumn(0).setMinWidth(0);
            tabListadoEquipos.getColumnModel().getColumn(0).setPreferredWidth(0);
            tabListadoEquipos.getColumnModel().getColumn(0).setMaxWidth(0);
            tabListadoEquipos.getColumnModel().getColumn(1).setPreferredWidth(150);
            tabListadoEquipos.getColumnModel().getColumn(7).setPreferredWidth(65);
            tabListadoEquipos.getColumnModel().getColumn(7).setMaxWidth(65);
            tabListadoEquipos.getColumnModel().getColumn(8).setPreferredWidth(65);
            tabListadoEquipos.getColumnModel().getColumn(8).setMaxWidth(65);
            tabListadoEquipos.getColumnModel().getColumn(13).setPreferredWidth(50);

            tabListadoEquipos.getColumnModel().getColumn(5).setPreferredWidth(100);
            tabListadoEquipos.getColumnModel().getColumn(6).setPreferredWidth(150);
            tabListadoEquipos.getColumnModel().getColumn(9).setPreferredWidth(150);
            tabListadoEquipos.getColumnModel().getColumn(10).setPreferredWidth(130);
            tabListadoEquipos.getColumnModel().getColumn(12).setPreferredWidth(100);
            tabListadoEquipos.getColumnModel().getColumn(14).setPreferredWidth(130);
            tabListadoEquipos.getColumnModel().getColumn(15).setMinWidth(0);
            tabListadoEquipos.getColumnModel().getColumn(15).setPreferredWidth(0);
            tabListadoEquipos.getColumnModel().getColumn(15).setMaxWidth(0);
            tabListadoEquipos.getColumnModel().getColumn(16).setPreferredWidth(100);
            tabListadoEquipos.getColumnModel().getColumn(17).setPreferredWidth(90);
            tabListadoEquipos.getColumnModel().getColumn(19).setPreferredWidth(90);
            //tabListadoEquipos.getColumnModel().getColumn(19).setMinWidth(120);
            //tabListadoEquipos.getColumnModel().getColumn(19).setMaxWidth(120);
        }

        btGuardarEquipo.setText("Guardar");
        btGuardarEquipo.setToolTipText("Guarda todo los registros modificados y nuevos");
        btGuardarEquipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btGuardarEquipoActionPerformed(evt);
            }
        });

        btEliminarEquipo.setText("Caducar");
        btEliminarEquipo.setToolTipText("Caduca un registro");
        btEliminarEquipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEliminarEquipoActionPerformed(evt);
            }
        });

        btNuevoEquipo.setText("Nuevo");
        btNuevoEquipo.setToolTipText("Agrega una nueva fila al final de la tabla");
        btNuevoEquipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNuevoEquipoActionPerformed(evt);
            }
        });

        btBuscarEquipos.setText("Buscar");
        btBuscarEquipos.setToolTipText("Buscar patrones: %búsqueda%");
        btBuscarEquipos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarEquiposActionPerformed(evt);
            }
        });

        tabBuscarEquipos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "IdActivo", "Cod. Activo", "Tipo", "Marca", "Modelo", "Serial", "Procesador", "RAM", "HDD", "Nombre", "Usuario", "MAC", "IP", "Estado", "Observación", "", "Oficina", "Departamento", "Ubicacion", "Fecha"
            }
        ));
        tabBuscarEquipos.setCellSelectionEnabled(true);
        tabBuscarEquipos.setToolTipText("Buscar patrones: %búsqueda%");
        tabBuscarEquipos.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane2.setViewportView(tabBuscarEquipos);
        if (tabBuscarEquipos.getColumnModel().getColumnCount() > 0) {
            tabBuscarEquipos.getColumnModel().getColumn(0).setMinWidth(0);
            tabBuscarEquipos.getColumnModel().getColumn(0).setPreferredWidth(0);
            tabBuscarEquipos.getColumnModel().getColumn(0).setMaxWidth(0);
            tabBuscarEquipos.getColumnModel().getColumn(1).setPreferredWidth(150);
            tabBuscarEquipos.getColumnModel().getColumn(7).setPreferredWidth(65);
            tabBuscarEquipos.getColumnModel().getColumn(8).setPreferredWidth(65);
            tabBuscarEquipos.getColumnModel().getColumn(13).setPreferredWidth(50);
            tabBuscarEquipos.getColumnModel().getColumn(15).setMinWidth(0);
            tabBuscarEquipos.getColumnModel().getColumn(15).setPreferredWidth(0);
            tabBuscarEquipos.getColumnModel().getColumn(15).setMaxWidth(0);
            tabBuscarEquipos.getColumnModel().getColumn(17).setPreferredWidth(90);
            tabBuscarEquipos.getColumnModel().getColumn(19).setPreferredWidth(90);
        }

        btAgregarGLPI.setText("+ GLPI");
        btAgregarGLPI.setToolTipText("Agregar GLPI a un equipo");
        btAgregarGLPI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAgregarGLPIActionPerformed(evt);
            }
        });

        btEquipoUserSoft.setText("Software");
        btEquipoUserSoft.setToolTipText("Asignar software a un equipo");
        btEquipoUserSoft.setEnabled(false);
        btEquipoUserSoft.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEquipoUserSoftActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpHardwareLayout = new javax.swing.GroupLayout(jpHardware);
        jpHardware.setLayout(jpHardwareLayout);
        jpHardwareLayout.setHorizontalGroup(
            jpHardwareLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpHardwareLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpHardwareLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btGuardarEquipo, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btEliminarEquipo, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btNuevoEquipo, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btBuscarEquipos, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btAgregarGLPI, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btEquipoUserSoft, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpHardwareLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1142, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1142, Short.MAX_VALUE))
                .addContainerGap())
        );
        jpHardwareLayout.setVerticalGroup(
            jpHardwareLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpHardwareLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpHardwareLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpHardwareLayout.createSequentialGroup()
                        .addComponent(btBuscarEquipos)
                        .addGap(47, 47, 47)
                        .addComponent(btNuevoEquipo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btGuardarEquipo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btEliminarEquipo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btAgregarGLPI)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btEquipoUserSoft)
                        .addGap(0, 191, Short.MAX_VALUE))
                    .addGroup(jpHardwareLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Inventario Hardware", jpHardware);

        btBuscarSoftwareEquipos.setText("Buscar");
        btBuscarSoftwareEquipos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarSoftwareEquiposActionPerformed(evt);
            }
        });

        tabListadoEquiposSoftware.setAutoCreateRowSorter(true);
        tabListadoEquiposSoftware.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "# Activo", "Nombre PC", "Fecha", "Técnico", "Usuario", "Estado", "Software", "Version", "Licencia"
            }
        ));
        jScrollPane8.setViewportView(tabListadoEquiposSoftware);
        if (tabListadoEquiposSoftware.getColumnModel().getColumnCount() > 0) {
            tabListadoEquiposSoftware.getColumnModel().getColumn(0).setPreferredWidth(150);
            tabListadoEquiposSoftware.getColumnModel().getColumn(1).setPreferredWidth(120);
            tabListadoEquiposSoftware.getColumnModel().getColumn(8).setPreferredWidth(230);
        }

        tabBuscarEquiposSoftware.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Cod. Activo", "Nombre PC", "Fecha", "Técnico", "Usuario", "Estado", "Software", "Versión", "Licencia"
            }
        ));
        tabBuscarEquiposSoftware.setCellSelectionEnabled(true);
        tabBuscarEquiposSoftware.setTableHeader(null);
        jScrollPane9.setViewportView(tabBuscarEquiposSoftware);
        if (tabBuscarEquiposSoftware.getColumnModel().getColumnCount() > 0) {
            tabBuscarEquiposSoftware.getColumnModel().getColumn(0).setMinWidth(150);
            tabBuscarEquiposSoftware.getColumnModel().getColumn(0).setPreferredWidth(150);
            tabBuscarEquiposSoftware.getColumnModel().getColumn(1).setPreferredWidth(150);
        }

        javax.swing.GroupLayout jpEquipoSoftwareLayout = new javax.swing.GroupLayout(jpEquipoSoftware);
        jpEquipoSoftware.setLayout(jpEquipoSoftwareLayout);
        jpEquipoSoftwareLayout.setHorizontalGroup(
            jpEquipoSoftwareLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpEquipoSoftwareLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btBuscarSoftwareEquipos, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jpEquipoSoftwareLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 1152, Short.MAX_VALUE)
                    .addComponent(jScrollPane9)))
        );
        jpEquipoSoftwareLayout.setVerticalGroup(
            jpEquipoSoftwareLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpEquipoSoftwareLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpEquipoSoftwareLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpEquipoSoftwareLayout.createSequentialGroup()
                        .addComponent(btBuscarSoftwareEquipos)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jpEquipoSoftwareLayout.createSequentialGroup()
                        .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 370, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Inventario Software", jpEquipoSoftware);

        tabListadoMateriales.setAutoCreateRowSorter(true);
        tabListadoMateriales.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Código", "Descripción", "Stock", "Entrada", "Salida"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, true, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabListadoMateriales.getTableHeader().setReorderingAllowed(false);
        jScrollPane11.setViewportView(tabListadoMateriales);
        if (tabListadoMateriales.getColumnModel().getColumnCount() > 0) {
            tabListadoMateriales.getColumnModel().getColumn(0).setMinWidth(0);
            tabListadoMateriales.getColumnModel().getColumn(0).setPreferredWidth(0);
            tabListadoMateriales.getColumnModel().getColumn(0).setMaxWidth(0);
            tabListadoMateriales.getColumnModel().getColumn(1).setMinWidth(70);
            tabListadoMateriales.getColumnModel().getColumn(1).setPreferredWidth(70);
            tabListadoMateriales.getColumnModel().getColumn(1).setMaxWidth(70);
            tabListadoMateriales.getColumnModel().getColumn(2).setPreferredWidth(200);
        }

        btNuevoMaterial.setText("Nuevo");
        btNuevoMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNuevoMaterialActionPerformed(evt);
            }
        });

        btRegistrar.setText("Guardar");
        btRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRegistrarActionPerformed(evt);
            }
        });

        btBuscarMaterial.setText("Buscar");
        btBuscarMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarMaterialActionPerformed(evt);
            }
        });

        tabBuscarMateriales.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        tabBuscarMateriales.setCellSelectionEnabled(true);
        tabBuscarMateriales.setTableHeader(null);
        jScrollPane15.setViewportView(tabBuscarMateriales);
        if (tabBuscarMateriales.getColumnModel().getColumnCount() > 0) {
            tabBuscarMateriales.getColumnModel().getColumn(0).setMinWidth(70);
            tabBuscarMateriales.getColumnModel().getColumn(0).setPreferredWidth(70);
            tabBuscarMateriales.getColumnModel().getColumn(0).setMaxWidth(70);
        }

        btEntradaMaterial.setText("+ Entrada");
        btEntradaMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEntradaMaterialActionPerformed(evt);
            }
        });

        btSalidaMaterial.setText("- Salida");
        btSalidaMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalidaMaterialActionPerformed(evt);
            }
        });

        btEliminarMaterial.setText("Caducar");
        btEliminarMaterial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEliminarMaterialActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(232, 232, 232)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btBuscarMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btNuevoMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btEliminarMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btSalidaMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btEntradaMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 552, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(373, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btBuscarMaterial))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(btNuevoMaterial)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btRegistrar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btEliminarMaterial)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btEntradaMaterial)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btSalidaMaterial)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 170, Short.MAX_VALUE)))
                .addGap(20, 20, 20))
        );

        jTabbedPane2.addTab("Material", jPanel9);

        tabListadoMovimientos.setAutoCreateRowSorter(true);
        tabListadoMovimientos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id Movimiento", "Fecha", "Descripción material", "Habia", "Movimiento", "Cantidad", "Observación"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane14.setViewportView(tabListadoMovimientos);
        if (tabListadoMovimientos.getColumnModel().getColumnCount() > 0) {
            tabListadoMovimientos.getColumnModel().getColumn(0).setMinWidth(0);
            tabListadoMovimientos.getColumnModel().getColumn(0).setPreferredWidth(0);
            tabListadoMovimientos.getColumnModel().getColumn(0).setMaxWidth(0);
            tabListadoMovimientos.getColumnModel().getColumn(1).setMinWidth(80);
            tabListadoMovimientos.getColumnModel().getColumn(2).setMinWidth(200);
            tabListadoMovimientos.getColumnModel().getColumn(3).setPreferredWidth(40);
            tabListadoMovimientos.getColumnModel().getColumn(4).setPreferredWidth(80);
            tabListadoMovimientos.getColumnModel().getColumn(5).setPreferredWidth(50);
            tabListadoMovimientos.getColumnModel().getColumn(6).setPreferredWidth(400);
        }

        btBuscarMovimiento.setText("Buscar");
        btBuscarMovimiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarMovimientoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(78, 78, 78)
                .addComponent(btBuscarMovimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jdcFechaMovimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDescMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 973, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(114, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btBuscarMovimiento)
                    .addComponent(jdcFechaMovimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDescMaterial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane14, javax.swing.GroupLayout.DEFAULT_SIZE, 343, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane2.addTab("Historial", jPanel12);

        javax.swing.GroupLayout jpMaterialesLayout = new javax.swing.GroupLayout(jpMateriales);
        jpMateriales.setLayout(jpMaterialesLayout);
        jpMaterialesLayout.setHorizontalGroup(
            jpMaterialesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );
        jpMaterialesLayout.setVerticalGroup(
            jpMaterialesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2)
        );

        jTabbedPane1.addTab("Inventario Materiales", jpMateriales);

        tabBuscarGLPI.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null}
            },
            new String [] {
                "Fecha", "Cod. Activo", "Title 3", "Técnico", "Num GLPI", "Descripción"
            }
        ));
        tabBuscarGLPI.setCellSelectionEnabled(true);
        tabBuscarGLPI.setTableHeader(null);
        jScrollPane12.setViewportView(tabBuscarGLPI);
        if (tabBuscarGLPI.getColumnModel().getColumnCount() > 0) {
            tabBuscarGLPI.getColumnModel().getColumn(0).setPreferredWidth(80);
            tabBuscarGLPI.getColumnModel().getColumn(0).setMaxWidth(80);
            tabBuscarGLPI.getColumnModel().getColumn(1).setPreferredWidth(150);
            tabBuscarGLPI.getColumnModel().getColumn(1).setMaxWidth(150);
            tabBuscarGLPI.getColumnModel().getColumn(2).setPreferredWidth(110);
            tabBuscarGLPI.getColumnModel().getColumn(3).setPreferredWidth(100);
            tabBuscarGLPI.getColumnModel().getColumn(4).setPreferredWidth(100);
            tabBuscarGLPI.getColumnModel().getColumn(4).setMaxWidth(150);
            tabBuscarGLPI.getColumnModel().getColumn(5).setPreferredWidth(600);
        }

        btBuscarGlpi.setText("Buscar");
        btBuscarGlpi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBuscarGlpiActionPerformed(evt);
            }
        });

        btEliminarGLPI.setText("Caducar");
        btEliminarGLPI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEliminarGLPIActionPerformed(evt);
            }
        });

        tabListadoGLPI.setAutoCreateRowSorter(true);
        tabListadoGLPI.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "IdGlpi", "IdEquipo", "Fecha", "Cod. Activo", "Tipo", "Técnico", "# GLPI", "Descripción"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true, true, true, true, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabListadoGLPI.getTableHeader().setReorderingAllowed(false);
        jScrollPane13.setViewportView(tabListadoGLPI);
        if (tabListadoGLPI.getColumnModel().getColumnCount() > 0) {
            tabListadoGLPI.getColumnModel().getColumn(0).setMinWidth(0);
            tabListadoGLPI.getColumnModel().getColumn(0).setPreferredWidth(0);
            tabListadoGLPI.getColumnModel().getColumn(0).setMaxWidth(0);
            tabListadoGLPI.getColumnModel().getColumn(1).setMinWidth(0);
            tabListadoGLPI.getColumnModel().getColumn(1).setPreferredWidth(0);
            tabListadoGLPI.getColumnModel().getColumn(1).setMaxWidth(0);
            tabListadoGLPI.getColumnModel().getColumn(2).setPreferredWidth(80);
            tabListadoGLPI.getColumnModel().getColumn(2).setMaxWidth(80);
            tabListadoGLPI.getColumnModel().getColumn(3).setPreferredWidth(150);
            tabListadoGLPI.getColumnModel().getColumn(3).setMaxWidth(150);
            tabListadoGLPI.getColumnModel().getColumn(4).setPreferredWidth(110);
            tabListadoGLPI.getColumnModel().getColumn(5).setPreferredWidth(100);
            tabListadoGLPI.getColumnModel().getColumn(6).setPreferredWidth(100);
            tabListadoGLPI.getColumnModel().getColumn(6).setMaxWidth(150);
            tabListadoGLPI.getColumnModel().getColumn(7).setPreferredWidth(600);
        }

        btModificarGLPI.setText("Editar");
        btModificarGLPI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btModificarGLPIActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpGLPILayout = new javax.swing.GroupLayout(jpGLPI);
        jpGLPI.setLayout(jpGLPILayout);
        jpGLPILayout.setHorizontalGroup(
            jpGLPILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpGLPILayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpGLPILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btEliminarGLPI, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btBuscarGlpi, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btModificarGLPI, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jpGLPILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane12, javax.swing.GroupLayout.DEFAULT_SIZE, 1142, Short.MAX_VALUE)
                    .addComponent(jScrollPane13))
                .addContainerGap())
        );
        jpGLPILayout.setVerticalGroup(
            jpGLPILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpGLPILayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpGLPILayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpGLPILayout.createSequentialGroup()
                        .addComponent(btBuscarGlpi)
                        .addGap(76, 76, 76)
                        .addComponent(btModificarGLPI)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btEliminarGLPI)
                        .addGap(0, 249, Short.MAX_VALUE))
                    .addGroup(jpGLPILayout.createSequentialGroup()
                        .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("GLPI", jpGLPI);

        jLabel8.setText("Usuario:");

        jLabel11.setText("Contraseña:");

        btIngresar.setText("Ingresar");
        btIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btIngresarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpLoginLayout = new javax.swing.GroupLayout(jpLogin);
        jpLogin.setLayout(jpLoginLayout);
        jpLoginLayout.setHorizontalGroup(
            jpLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpLoginLayout.createSequentialGroup()
                .addGap(517, 517, 517)
                .addGroup(jpLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(jLabel8))
                .addGap(18, 18, 18)
                .addGroup(jpLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btIngresar)
                    .addGroup(jpLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtContraseña)
                        .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(550, Short.MAX_VALUE))
        );
        jpLoginLayout.setVerticalGroup(
            jpLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpLoginLayout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addGroup(jpLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btIngresar)
                .addContainerGap(274, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Login", jpLogin);

        panelGlobal.add(jTabbedPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtMsg, javax.swing.GroupLayout.DEFAULT_SIZE, 1208, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtLabelUsuario))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(panelGlobal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(457, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtLabelUsuario)
                    .addComponent(txtMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(panelGlobal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(21, 21, 21)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btIngresarActionPerformed
        // TODO add your handling code here:
        usuario = new UsuarioLab();
        ConsultasUsuariosLab conUsuariosLab = new ConsultasUsuariosLab();

        Date date = new Date();
        DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String contraseña = new String(txtContraseña.getPassword());

        if (!txtUsuario.getText().equals("") && !contraseña.equals("")) {

            String nuevaContraseña = Hash.sha1(contraseña);
            usuario.setUsuario(txtUsuario.getText());
            usuario.setContraseña(nuevaContraseña);
            usuario.setUltima_sesion(fechaHora.format(date));

            if (conUsuariosLab.login(usuario)) {
                jTabbedPane1.removeAll();
                jTabbedPane1.addTab("Inventario Hardware", jpHardware);
                jTabbedPane1.addTab("Inventario Software", jpEquipoSoftware);
                jTabbedPane1.addTab("Inventario Materiales", jpMateriales);
                jTabbedPane1.addTab("GLPI", jpGLPI);

                cusuario = txtUsuario.getText();
                czona = usuario.getId_zona();
                txtLabelUsuario.setText(txtUsuario.getText()+" - " + usuario.getNombre_zona() + "   ");

                btMenu.setVisible(true);

                llenarComboBoxTipoHardware();
                llenarComboBoxMarca();
                llenarComboBoxOficinas();
                llenarComboBoxDepartamentos();

                if (usuario.getNombre_tipo().equals("Administrador")) {
                    menuUsuariosLab.setVisible(true);
                    menuRespaldarDB.setVisible(true);
                    menuRestaurarDB.setVisible(true);

                }

            } else {
                JOptionPane.showMessageDialog(null, "Datos incorrectos");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe ingresar sus datos");
        }
    }//GEN-LAST:event_btIngresarActionPerformed

    private void btBuscarSoftwareEquiposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarSoftwareEquiposActionPerformed
        // TODO add your handling code here:
        conSoft.buscarHistorialSotware(tabBuscarEquiposSoftware, tabListadoEquiposSoftware);
    }//GEN-LAST:event_btBuscarSoftwareEquiposActionPerformed

    private void btBuscarMovimientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarMovimientoActionPerformed
        // TODO add your handling code here:
        Date date = jdcFechaMovimiento.getDate();
        java.sql.Date fecha = null;
        if (date != null) {
            long d = date.getTime();
            fecha = new java.sql.Date(d);
        }
        conMateriales.buscarMovimiento(fecha, txtDescMaterial.getText(), tabListadoMovimientos, modeloTabMovimientos);
    }//GEN-LAST:event_btBuscarMovimientoActionPerformed

    private void btEliminarMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEliminarMaterialActionPerformed
        // TODO add your handling code here:
        int j = tabListadoMateriales.getSelectedRow();
        if (j >= 0) {
            j = tabListadoMateriales.convertRowIndexToModel(j);
            if (tabListadoMateriales.getModel().getValueAt(j, 0) != null) {
                int a = JOptionPane.showConfirmDialog(null, "Esta seguro de caducar " + (String) tabListadoMateriales.getModel().getValueAt(j, 2));
                if (a == JOptionPane.YES_OPTION) {
                    material.setId((int) tabListadoMateriales.getModel().getValueAt(j, 0));
                    conMateriales.caducar(material);
                    txtMsg.setText("Material " + (String) tabListadoMateriales.getModel().getValueAt(j, 2) + " caducado.");

                    modeloTabMateriales.removeRow(j);
                    registroTablaMateriales = actualizarFilaControlRegistro(registroTablaMateriales, j);
                }

            } else if (tabListadoMateriales.getModel().getValueAt(j, 0) == null) {
                modeloTabMateriales.removeRow(j);
                registroTablaMateriales = actualizarFilaControlRegistro(registroTablaMateriales, j);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar una fila.", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btEliminarMaterialActionPerformed

    private void btSalidaMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalidaMaterialActionPerformed
        // TODO add your handling code here:
        int pos = tabListadoMateriales.getSelectedRow();
        if (pos >= 0) {
            pos = tabListadoMateriales.convertRowIndexToModel(pos);
            if ((int) tabListadoMateriales.getModel().getValueAt(pos, 3) > 0) {
                //int t=(int) tabListadoMateriales.getModel().getValueAt(pos, 0);
                vMovimientoMaterial ventana = new vMovimientoMaterial(this, true);
                material.setId((int) tabListadoMateriales.getModel().getValueAt(pos, 0));
                material.setCodigo((String) tabListadoMateriales.getModel().getValueAt(pos, 2));
                int stock = (int) tabListadoMateriales.getModel().getValueAt(pos, 3);
                ventana.material = this.material;
                ventana.conMateriales = this.conMateriales;
                ventana.stock = stock;
                ventana.labelStock.setText("" + stock);
                ventana.movimiento = "Salida";
                ventana.setTitle("SALIDA: " + (String) tabListadoMateriales.getModel().getValueAt(pos, 2));
                ventana.tabListado = this.tabListadoMateriales;
                ventana.pos = pos;
                ventana.txtMsg = this.txtMsg;
                ventana.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "Actualmente no hay stock de este material", "Alerta", JOptionPane.WARNING_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un material.", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btSalidaMaterialActionPerformed

    private void btEntradaMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEntradaMaterialActionPerformed
        // TODO add your handling code here:
        int pos = tabListadoMateriales.getSelectedRow();
        if (pos >= 0) {
            pos = tabListadoMateriales.convertRowIndexToModel(pos);
            if (tabListadoMateriales.getModel().getValueAt(pos, 0) != null) {
                //int t=(int) tabListadoMateriales.getValueAt(pos, 0);
                vMovimientoMaterial ventana = new vMovimientoMaterial(this, true);
                material.setId((int) tabListadoMateriales.getModel().getValueAt(pos, 0));
                material.setCodigo((String) tabListadoMateriales.getModel().getValueAt(pos, 2));
                int stock = (int) tabListadoMateriales.getModel().getValueAt(pos, 3);
                ventana.material = this.material;
                ventana.conMateriales = this.conMateriales;
                ventana.stock = stock;
                ventana.labelStock.setText("" + stock);
                ventana.movimiento = "Entrada";
                ventana.setTitle("INGRESO: " + (String) tabListadoMateriales.getModel().getValueAt(pos, 2));
                ventana.tabListado = this.tabListadoMateriales;
                ventana.pos = pos;
                ventana.txtMsg = this.txtMsg;
                ventana.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "Material aun no guardado.", "Alerta", JOptionPane.WARNING_MESSAGE);
            }

        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un material.", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btEntradaMaterialActionPerformed

    private void btBuscarMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarMaterialActionPerformed
        // TODO add your handling code here:
        if (registroTablaMateriales.size() > 0) {
            int a = JOptionPane.showConfirmDialog(null, "Hay cambios no guardados, ¿desea continuar? ");
            if (a == JOptionPane.YES_OPTION) {
                llenarDatosTablaMateriales();
            }
        } else {
            llenarDatosTablaMateriales();
        }
        txtMsg.setText("Materiales zona " + usuario.getNombre_zona());
    }//GEN-LAST:event_btBuscarMaterialActionPerformed

    public void llenarDatosTablaMateriales() {
        material.setCodigo((String) tabBuscarMateriales.getModel().getValueAt(0, 0));
        material.setDescripcion((String) tabBuscarMateriales.getModel().getValueAt(0, 1));
        //conMateriales.buscar(material, tabListadoMateriales, modeloTabMateriales);
        conMateriales.calcularStock(material, tabListadoMateriales, modeloTabMateriales);
    }

    private void btRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRegistrarActionPerformed
        // TODO add your handling code here:
        ControlTabla[] c = new ControlTabla[registroTablaMateriales.size()];
        for (int i = 0; i < registroTablaMateriales.size(); i++) {
            c[i] = registroTablaMateriales.get(i);
        }
        registroTablaMateriales.clear();
        System.out.println("Object array before sorting : " + Arrays.toString(c));
        Arrays.sort(c, new ControlTabla.FilaColumnaComparator());
        System.out.println("Object array after sorting in natural order : " + Arrays.toString(c));

        int filaRegistrada = -1;
        for (ControlTabla c1 : c) {
            if (c1.fila < tabListadoMateriales.getRowCount()) {
                if (filaRegistrada != c1.fila) {//Se control el registro
                    if (!tabListadoMateriales.getModel().getValueAt(c1.fila, 1).equals("") && !tabListadoMateriales.getModel().getValueAt(c1.fila, 2).equals("")) {
                        material.setCodigo((String) tabListadoMateriales.getModel().getValueAt(c1.fila, 1));
                        material.setDescripcion((String) tabListadoMateriales.getModel().getValueAt(c1.fila, 2));
                        if (tabListadoMateriales.getModel().getValueAt(c1.fila, 0) == null) { //Nuevo registro
                            int idMaterial = conMateriales.buscarId(material);
                            if (idMaterial == -1) {
                                if (conMateriales.registrar(material)) {
                                    idMaterial = conMateriales.buscarId(material);
                                    tabListadoMateriales.getModel().setValueAt(idMaterial, c1.fila, 0);
                                    txtMsg.setText("Nuevo material " + (String) tabListadoMateriales.getModel().getValueAt(c1.fila, 2) + " guardado");
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Ya existe un material con el codigo " + material.getCodigo(), "Alerta", JOptionPane.WARNING_MESSAGE);
                            }
                        } else {//Registro a modificar
                            material.setId((int) tabListadoMateriales.getModel().getValueAt(c1.fila, 0));
                            int existeOtro = conMateriales.existeOtroMaterial(material);
                            if (existeOtro == 0) {
                                if (conMateriales.modificar(material)) {
                                    txtMsg.setText("Material " + material.getCodigo() + " modificado.");
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Ya existe un material con el codigo " + material.getCodigo(), "Alerta", JOptionPane.WARNING_MESSAGE);
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Hay campos vacios en la fila " + c1.fila, "Alerta", JOptionPane.WARNING_MESSAGE);
                    }
                    filaRegistrada = c1.fila;
                }
            }
        }

    }//GEN-LAST:event_btRegistrarActionPerformed

    private void btNuevoMaterialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNuevoMaterialActionPerformed
        // TODO add your handling code here:
        tabListadoMateriales.getRowSorter().setSortKeys(null);
        modeloTabMateriales.addRow(new Object[]{null, "", "", 0, 0, 0});

        int i = tabListadoMateriales.getRowCount() - 1;
        tabListadoMateriales.setRowSelectionInterval(i, i);
        tabListadoMateriales.scrollRectToVisible(new Rectangle(tabListadoMateriales.getCellRect(i, 0, true)));
    }//GEN-LAST:event_btNuevoMaterialActionPerformed

    private void btEliminarGLPIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEliminarGLPIActionPerformed
        // TODO add your handling code here:
        int j = tabListadoGLPI.getSelectedRow();
        if (j >= 0) {
            j = tabListadoGLPI.convertRowIndexToModel(j);
            if (tabListadoGLPI.getModel().getValueAt(j, 0) != null) {
                int a = JOptionPane.showConfirmDialog(null, "Esta seguro de caducar el GLPI " + (String) tabListadoGLPI.getModel().getValueAt(j, 6));
                if (a == JOptionPane.YES_OPTION) {
                    glpi.setId((int) tabListadoGLPI.getModel().getValueAt(j, 0));
                    if (conGLPI.eliminar(glpi)) {
                        txtMsg.setText("GLPI " + (String) tabListadoGLPI.getModel().getValueAt(j, 6) + " caducado.");
                        modeloTabGlpi.removeRow(j);
                    }
                }

            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un equipo.", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btEliminarGLPIActionPerformed

    private void btBuscarGlpiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarGlpiActionPerformed
        // TODO add your handling code here:
        equipo.setCodActivo((String) tabBuscarGLPI.getModel().getValueAt(0, 1));
        equipo.setTipo((String) tabBuscarGLPI.getModel().getValueAt(0, 2));

        glpi.setFecha((String) tabBuscarGLPI.getModel().getValueAt(0, 0));
        glpi.setTecnico((String) tabBuscarGLPI.getModel().getValueAt(0, 3));
        glpi.setNum_glpi((String) tabBuscarGLPI.getModel().getValueAt(0, 4));
        glpi.setDescripcion((String) tabBuscarGLPI.getModel().getValueAt(0, 5));
        conGLPI.buscarEquipoGLpi(equipo, glpi, tabListadoGLPI);
    }//GEN-LAST:event_btBuscarGlpiActionPerformed

    private void btEquipoUserSoftActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEquipoUserSoftActionPerformed
        // TODO add your handling code here:
        int pos = tabListadoEquipos.getSelectedRow();
        if (pos >= 0) {
            pos = tabListadoEquipos.convertRowIndexToModel(pos);
            vAsignarSoftware ventana = new vAsignarSoftware(this, true);
            equipo.setId((int) tabListadoEquipos.getModel().getValueAt(pos, 0));
            ventana.equipo = this.equipo;
            ventana.txtCodActivo.setText((String) tabListadoEquipos.getModel().getValueAt(pos, 1));
            ventana.conSoft = this.conSoft;
            ventana.cargarSoftwareInstalado(equipo);
            ventana.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un equipo.", "Equipo", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btEquipoUserSoftActionPerformed

    private void btAgregarGLPIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAgregarGLPIActionPerformed
        // TODO add your handling code here:
        int pos = tabListadoEquipos.getSelectedRow();
        if (pos >= 0) {
            pos = tabListadoEquipos.convertRowIndexToModel(pos);
            vAgregarGLPI ventana = new vAgregarGLPI(this, true);
            equipo.setId((int) tabListadoEquipos.getModel().getValueAt(pos, 0));
            ventana.equipo = this.equipo;
            ventana.glpi = this.glpi;
            ventana.conGLPI = this.conGLPI;
            ventana.pestaña = true;
            ventana.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un equipo.", "GLPI", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btAgregarGLPIActionPerformed

    private void btBuscarEquiposActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBuscarEquiposActionPerformed
        // TODO add your handling code here:     
        if (registroTablaEquipos.size() > 0) {
            int a = JOptionPane.showConfirmDialog(null, "Hay cambios no guardados, ¿desea continuar? ");
            if (a == JOptionPane.YES_OPTION) {
                llenarDatosTablaEquipos();
            }
        } else {
            llenarDatosTablaEquipos();
            llenarComboBoxTipoHardware();
            llenarComboBoxMarca();
        }

    }//GEN-LAST:event_btBuscarEquiposActionPerformed

    public void llenarDatosTablaEquipos() {
        registroTablaEquipos.clear();
        equipo.setCodActivo((String) tabBuscarEquipos.getValueAt(0, 1));
        equipo.setTipo((String) tabBuscarEquipos.getValueAt(0, 2));
        equipo.setMarca((String) tabBuscarEquipos.getValueAt(0, 3));
        equipo.setModelo((String) tabBuscarEquipos.getValueAt(0, 4));
        equipo.setSerial((String) tabBuscarEquipos.getValueAt(0, 5));
        equipo.setProcesador((String) tabBuscarEquipos.getValueAt(0, 6));
        String ram = (String) tabBuscarEquipos.getValueAt(0, 7);
        equipo.setRam(ram == null || ram.length() == 0 ? 0 : Integer.parseInt(ram));
        String discoduro = (String) tabBuscarEquipos.getValueAt(0, 8);
        equipo.setDiscoduro(discoduro == null || discoduro.length() == 0 ? 0 : Integer.parseInt(discoduro));
        equipo.setNombre((String) tabBuscarEquipos.getValueAt(0, 9));
        equipo.setMac((String) tabBuscarEquipos.getValueAt(0, 11));
        equipo.setIp((String) tabBuscarEquipos.getValueAt(0, 12));
        equipo.setEstado((String) tabBuscarEquipos.getValueAt(0, 13));
        equipo.setObservacion((String) tabBuscarEquipos.getValueAt(0, 14));
        equipo.setUbicacion((String) tabBuscarEquipos.getValueAt(0, 18));
        oficina.setNombre((String) tabBuscarEquipos.getValueAt(0, 16));
        departamento.setNombre((String) tabBuscarEquipos.getValueAt(0, 17));
        String fecha = (String) tabBuscarEquipos.getValueAt(0, 19);
        String usuarioPC = (String) tabBuscarEquipos.getValueAt(0, 10);

        //conEquipos.buscar(equipo, tabListadoEquipos);
        conOficinas.buscarEquipos2(equipo, oficina, departamento, usuarioPC, fecha, tabListadoEquipos);
        llenarComboBoxOficinas();
        llenarComboBoxDepartamentos();
    }

    private void btNuevoEquipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNuevoEquipoActionPerformed
        // TODO add your handling code here:
        tabListadoEquipos.getRowSorter().setSortKeys(null);
        modeloTabEquipos.addRow(new Object[]{null, null, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""});

        int i = tabListadoEquipos.getRowCount() - 1;
        tabListadoEquipos.setRowSelectionInterval(i, i);
        tabListadoEquipos.scrollRectToVisible(new Rectangle(tabListadoEquipos.getCellRect(i, 0, true)));

        llenarComboBoxOficinas();
        llenarComboBoxDepartamentos();

    }//GEN-LAST:event_btNuevoEquipoActionPerformed

    private void btEliminarEquipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEliminarEquipoActionPerformed
        // TODO add your handling code here:
        int j = tabListadoEquipos.getSelectedRow();
        if (j >= 0) {
            j = tabListadoEquipos.convertRowIndexToModel(j);
            if (tabListadoEquipos.getModel().getValueAt(j, 0) != null) {
                int a = JOptionPane.showConfirmDialog(null, "Esta seguro de caducar el equipo " + (String) tabListadoEquipos.getModel().getValueAt(j, 1));
                if (a == JOptionPane.YES_OPTION) {
                    equipo.setId((int) tabListadoEquipos.getModel().getValueAt(j, 0));
                    conEquipos.eliminar(equipo);
                    txtMsg.setText("Equipo " + (String) tabListadoEquipos.getModel().getValueAt(j, 1) + " caducado");

                    if ((int) tabListadoEquipos.getModel().getValueAt(j, 14) != 0) {
                        conOficinas.eliminarEquipo(equipo);
                    }
                    modeloTabEquipos.removeRow(j);
                    registroTablaEquipos = actualizarFilaControlRegistro(registroTablaEquipos, j);
                }

            } else if (tabListadoEquipos.getModel().getValueAt(j, 0) == null) {
                modeloTabEquipos.removeRow(j);
                registroTablaEquipos = actualizarFilaControlRegistro(registroTablaEquipos, j);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un equipo.", "Alerta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_btEliminarEquipoActionPerformed

    public List<ControlTabla> actualizarFilaControlRegistro(List<ControlTabla> registroTabla, int posEliminado) {
        //ordenar filas en registro
        ControlTabla[] c = new ControlTabla[registroTabla.size()];
        for (int i = 0; i < registroTabla.size(); i++) {
            c[i] = registroTabla.get(i);
        }
        registroTabla.clear();
        System.out.println("Object array before sorting : " + Arrays.toString(c));
        Arrays.sort(c, new ControlTabla.FilaColumnaComparator());
        System.out.println("Object array after sorting in natural order : " + Arrays.toString(c));
        for (ControlTabla c1 : c) {
            if (c1.fila != posEliminado) {
                if (c1.fila > posEliminado) {
                    c1.fila--;
                }

                registroTabla.add(c1);
            }
        }
        System.out.println("Object array dspues de eliminar : " + registroTabla.toString());
        return registroTabla;
    }

    private void btGuardarEquipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btGuardarEquipoActionPerformed
        // TODO add your handling code here:
        ControlTabla[] c = new ControlTabla[registroTablaEquipos.size()];
        for (int i = 0; i < registroTablaEquipos.size(); i++) {
            c[i] = registroTablaEquipos.get(i);
        }
        registroTablaEquipos.clear();
        System.out.println("Object array before sorting : " + Arrays.toString(c));
        Arrays.sort(c, new ControlTabla.FilaColumnaComparator());
        System.out.println("Object array after sorting in natural order : " + Arrays.toString(c));

        int filaEquipoRegistrado = -1;
        int filaOficinaRegistrado = -1;
        int filaDepartamentoRegistrado = -1;

        for (ControlTabla c1 : c) {
            if (c1.fila < tabListadoEquipos.getRowCount()) {
                if ((c1.columna < 14 || c1.columna==18) && filaEquipoRegistrado != c1.fila) {//Se control el registro de equipos
                    equipo.setCodActivo((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 1));
                    equipo.setTipo((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 2));
                    equipo.setMarca((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 3));
                    equipo.setModelo((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 4));
                    equipo.setSerial((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 5));
                    equipo.setProcesador((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 6));

                    String ram = tabListadoEquipos.getModel().getValueAt(c1.fila, 7).toString();
                    String discoduro = tabListadoEquipos.getModel().getValueAt(c1.fila, 8).toString();

                    try {
                        equipo.setRam(ram == null || ram.length() == 0 || ram.equals("0") ? null : Integer.parseInt(ram));
                        equipo.setDiscoduro(discoduro == null || discoduro.length() == 0 || discoduro.equals("0") ? null : Integer.parseInt(discoduro));

                        equipo.setNombre((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 9));
                        equipo.setMac((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 11));
                        equipo.setIp((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 12));
                        equipo.setEstado((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 13));
                        equipo.setObservacion((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 14));
                        equipo.setUbicacion((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 18));

                        if (tabListadoEquipos.getModel().getValueAt(c1.fila, 0) == null) {//valor a guardar
                            int idEquipo = conEquipos.buscarIdEquipo(equipo);
                            if (idEquipo == -1) {
                                idEquipo = conEquipos.registrar(equipo);
                                if (idEquipo != -1) {
                                    txtMsg.setText("Equipo " + equipo.getCodActivo() + " guardado.");
                                    tabListadoEquipos.getModel().setValueAt(idEquipo, c1.fila, 0);
                                }
                            } else {
                                int a = JOptionPane.showConfirmDialog(null, "Ya existe un equipo con el codigo de activo: " + equipo.getCodActivo() + "\nDesea actualizarlo?");
                                if (a == JOptionPane.YES_OPTION) {
                                    equipo.setId(idEquipo);
                                    if (conEquipos.modificar(equipo)) {
                                        txtMsg.setText("Equipo " + equipo.getCodActivo() + " modificado.");
                                        tabListadoEquipos.getModel().setValueAt(idEquipo, c1.fila, 0);
                                    }
                                } else {
                                    for (int i = 0; i < c.length; i++) {
                                        if (c[i].fila == c1.fila) {
                                            registroTablaEquipos.add(c[i]);
                                        }
                                    }
                                }

                            }

                        } else {//fila a modificar
                            equipo.setId((int) tabListadoEquipos.getModel().getValueAt(c1.fila, 0));
                            if (conEquipos.modificar(equipo)) {
                                txtMsg.setText("Equipo " + equipo.getCodActivo() + " modificado.");
                            }
                        }
                    } catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(null, "Los campos RAM y HDD deben contener solo numeros", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    filaEquipoRegistrado = c1.fila;
                } else if (c1.columna == 16 && filaOficinaRegistrado != c1.fila) { //Se control el registro de oficinas
                    if (tabListadoEquipos.getModel().getValueAt(c1.fila, 0) != null) {
                        oficina = (Oficina) tabListadoEquipos.getModel().getValueAt(c1.fila, 16);
                        int idEquipo = (int) tabListadoEquipos.getModel().getValueAt(c1.fila, 0);

                        if (!(tabListadoEquipos.getValueAt(c1.fila, 17) instanceof String)) {
                            departamento = (Departamento) tabListadoEquipos.getModel().getValueAt(c1.fila, 17);
                        } else {
                            departamento.setNombre((String) tabListadoEquipos.getModel().getValueAt(c1.fila, 17));
                        }

                        if (conOficinas.existeEquipoOficina(idEquipo) == 0) {
                            System.out.println("nueva oficina asignada");
                            conOficinas.registrarEquipoOficina(oficina.getId(), idEquipo, departamento.getNombre());

                        } else {
                            System.out.println("se modifica oficina");
                            conOficinas.modificarEquipoOficina(oficina.getId(), idEquipo, departamento.getNombre());
                        }
                    }

                    filaOficinaRegistrado = c1.fila;
                    filaDepartamentoRegistrado = c1.fila;

                } else if (c1.columna == 17 && filaDepartamentoRegistrado != c1.fila && (tabListadoEquipos.getModel().getValueAt(c1.fila, 16)).toString().length() > 0) {
                    if (tabListadoEquipos.getModel().getValueAt(c1.fila, 0) != null) { //Control registro departamentos
                        oficina = (Oficina) tabListadoEquipos.getModel().getValueAt(c1.fila, 16);
                        int idEquipo = (int) tabListadoEquipos.getModel().getValueAt(c1.fila, 0);
                        departamento = (Departamento) tabListadoEquipos.getModel().getValueAt(c1.fila, 17);
                        if (conOficinas.existeDepartamentoOficina(idEquipo, oficina.getId(), departamento.getNombre()) == 0) {
                            System.out.println("Se modifica departamento");
                            conOficinas.modificarEquipoOficina(oficina.getId(), idEquipo, departamento.getNombre());
                        }
                    }
                    filaDepartamentoRegistrado = c1.fila;
                }

            }
        }

    }//GEN-LAST:event_btGuardarEquipoActionPerformed

    private void btMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btMenuActionPerformed
        // TODO add your handling code here:
        jPopupMenu1.show(btMenu, 0, btMenu.getHeight());
    }//GEN-LAST:event_btMenuActionPerformed

    private void menuOficinasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuOficinasActionPerformed
        // TODO add your handling code here:
        vOficinas ventana = new vOficinas(null, true);
        ventana.setVisible(true);
        llenarComboBoxOficinas();
    }//GEN-LAST:event_menuOficinasActionPerformed

    private void menuDepartamentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDepartamentosActionPerformed
        // TODO add your handling code here:
        vDepartamentos ventana = new vDepartamentos(null, true);
        ventana.setVisible(true);
        llenarComboBoxDepartamentos();
    }//GEN-LAST:event_menuDepartamentosActionPerformed

    private void menuSoftwareActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSoftwareActionPerformed
        // TODO add your handling code here:
        vSoftware ventana = new vSoftware(null, true);
        ventana.setVisible(true);
    }//GEN-LAST:event_menuSoftwareActionPerformed

    private void menuUsuariosPCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsuariosPCActionPerformed
        // TODO add your handling code here:
        vUsuariosPC ventana = new vUsuariosPC(null, true);
        ventana.setVisible(true);
    }//GEN-LAST:event_menuUsuariosPCActionPerformed

    private void menuUsuariosLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuUsuariosLabActionPerformed
        // TODO add your handling code here:
        vUsuariosLab ventana = new vUsuariosLab(this, true);
        ventana.setVisible(true);
    }//GEN-LAST:event_menuUsuariosLabActionPerformed

    private void menuReportesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuReportesActionPerformed
        // TODO add your handling code here:
        vReportes ventana = new vReportes(this, true);
        ventana.usuario=this.usuario;
        ventana.setVisible(true);
    }//GEN-LAST:event_menuReportesActionPerformed

    private void menuAcercaDeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAcercaDeActionPerformed
        // TODO add your handling code here:
        vAcercaDe ventana = new vAcercaDe(null, true);
        ventana.setVisible(true);
    }//GEN-LAST:event_menuAcercaDeActionPerformed

    private void menuSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSalirActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_menuSalirActionPerformed

    private void menuRespaldarDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRespaldarDBActionPerformed
        // TODO add your handling code here:
        try {
            Process p = Runtime.getRuntime().exec("mysqldump --host 172.18.5.22 -u root -plabJA2020 db_inventario");
            new HiloLector(p.getErrorStream()).start();
            InputStream is = p.getInputStream();

            String nombreRespaldo;
            Date date = new Date();
            DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            nombreRespaldo = fechaHora.format(date);
            nombreRespaldo = nombreRespaldo + " inventarioJA.sql";

            FileOutputStream fos = new FileOutputStream(nombreRespaldo);
            byte[] buffer = new byte[1000];
            int leido = is.read(buffer);
            while (leido > 0) {
                fos.write(buffer, 0, leido);
                leido = is.read(buffer);
            }
            fos.close();
            txtMsg.setText("Respaldo creado: " + nombreRespaldo);
        } catch (IOException ex) {
            Logger.getLogger(ventanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_menuRespaldarDBActionPerformed

    private void menuRestaurarDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRestaurarDBActionPerformed
        // TODO add your handling code here:
        try {
            Process p = Runtime.getRuntime().exec("mysql -u root -plabJA2020 prueba_respaldo");
            new HiloLector(p.getErrorStream()).start();
            OutputStream os = p.getOutputStream();
            FileInputStream fis = new FileInputStream("respaldo_inventarioJA.sql");
            byte[] buffer = new byte[1000];
            int leido = fis.read(buffer);
            while (leido > 0) {
                os.write(buffer, 0, leido);
                leido = fis.read(buffer);
            }
            os.flush();
            os.close();
            fis.close();
        } catch (IOException ex) {
            Logger.getLogger(ventanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_menuRestaurarDBActionPerformed

    private void menuCerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCerrarSesionActionPerformed
        // TODO add your handling code here:
        limpiarInterfaz();
        txtUsuario.setText("");
        txtContraseña.setText("");
        txtLabelUsuario.setText("");
        txtMsg.setText("");
    }//GEN-LAST:event_menuCerrarSesionActionPerformed

    private void btModificarGLPIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btModificarGLPIActionPerformed
        // TODO add your handling code here:
        int pos = tabListadoGLPI.getSelectedRow();
        if (pos >= 0) {
            pos = tabListadoGLPI.convertRowIndexToModel(pos);
            try {
                vAgregarGLPI ventana = new vAgregarGLPI(this, true);
                equipo.setId((int) tabListadoGLPI.getModel().getValueAt(pos, 1));
                ventana.equipo = this.equipo;
                glpi.setId((int) tabListadoGLPI.getModel().getValueAt(pos, 0));
                ventana.glpi = this.glpi;
                ventana.conGLPI = this.conGLPI;
                ventana.jtxtGLPI.setText((String) tabListadoGLPI.getModel().getValueAt(pos, 6));
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse((String) tabListadoGLPI.getModel().getValueAt(pos, 2));
                ventana.jdcFecha.setDate(date1);
                ventana.jtxtDescripcion.setText((String) tabListadoGLPI.getModel().getValueAt(pos, 7));
                ventana.pestaña = false;
                ventana.tabListadoGLPI = this.tabListadoGLPI;
                ventana.pos = pos;

                ventana.setVisible(true);

            } catch (ParseException ex) {
                Logger.getLogger(ventanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un equipo.", "GLPI", JOptionPane.WARNING_MESSAGE);
        }

    }//GEN-LAST:event_btModificarGLPIActionPerformed
    private void tabListadoEquiposListaSeleccionada(ListSelectionEvent evt) {
        int pos = tabListadoEquipos.getSelectedRow();
        if (pos >= 0) {
            pos = tabListadoEquipos.convertRowIndexToModel(pos);
            if ((tabListadoEquipos.getModel().getValueAt(pos, 2).equals("CPU") || tabListadoEquipos.getModel().getValueAt(pos, 2).equals("PORTÁTIL") || tabListadoEquipos.getModel().getValueAt(pos, 2).equals("ALL IN ONE")) && tabListadoEquipos.getModel().getValueAt(pos, 0) != null) {
                btEquipoUserSoft.setEnabled(true);
            } else {
                btEquipoUserSoft.setEnabled(false);
            }
        }
    }

    public void llenarComboBoxOficinas() {
        TableColumn columnaOficina = tabListadoEquipos.getColumnModel().getColumn(16);
        comboBoxOficina = conOficinas.llenarCombBox();
        comboBoxOficina.addActionListener(new ActionListener() {//add actionlistner to listen for change
            @Override
            public void actionPerformed(ActionEvent e) {
                int fila = tabListadoEquipos.getSelectedRow();
                if (fila >= 0) {
                    fila = tabListadoEquipos.convertRowIndexToModel(fila);
                    if (tabListadoEquipos.getModel().getValueAt(fila, 17).toString().equals("")) {
                        departamento.setNombre("SIN ASIGNAR");
                        tabListadoEquipos.getModel().setValueAt(departamento, fila, 17);
                    }
                }
            }
        });
        columnaOficina.setCellEditor(new DefaultCellEditor(comboBoxOficina));
    }

    public void llenarComboBoxDepartamentos() {
        TableColumn columnaDepartamento = tabListadoEquipos.getColumnModel().getColumn(17);
        comboBoxDepartamento = conDepartamento.llenarCombBox();
        columnaDepartamento.setCellEditor(new DefaultCellEditor(comboBoxDepartamento));
    }

    public void llenarComboBoxTipoHardware() {
        TableColumn columnaTipo = tabListadoEquipos.getColumnModel().getColumn(2);
        comboBoxTipoHardware = conTipoHardware.llenarComboBox();
        columnaTipo.setCellEditor(new DefaultCellEditor(comboBoxTipoHardware));
    }

    public void llenarComboBoxMarca() {
        TableColumn columnaMarca = tabListadoEquipos.getColumnModel().getColumn(3);
        comboBoxMarcaHardware = conTipoHardware.llenarComboBoxMarca();
        columnaMarca.setCellEditor(new DefaultCellEditor(comboBoxMarcaHardware));
    }

    public void llenarComboBoxTécnico() {
//        UsuarioLab usuario = new UsuarioLab();
//        ConsultasUsuariosLab conUsuariosLab = new ConsultasUsuariosLab();
//        
//        TableColumn columnaTecnico = tabBuscarGLPI.getColumnModel().getColumn(3);
//        comboBoxTecnico = conUsuariosLab.llenarCombBox();
//        columnaTecnico.setCellEditor(new DefaultCellEditor(comboBoxTecnico));
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ventanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ventanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ventanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ventanaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ventanaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAgregarGLPI;
    private javax.swing.JButton btBuscarEquipos;
    private javax.swing.JButton btBuscarGlpi;
    private javax.swing.JButton btBuscarMaterial;
    private javax.swing.JButton btBuscarMovimiento;
    private javax.swing.JButton btBuscarSoftwareEquipos;
    private javax.swing.JButton btEliminarEquipo;
    private javax.swing.JButton btEliminarGLPI;
    private javax.swing.JButton btEliminarMaterial;
    private javax.swing.JButton btEntradaMaterial;
    private javax.swing.JButton btEquipoUserSoft;
    private javax.swing.JButton btGuardarEquipo;
    private javax.swing.JButton btIngresar;
    private javax.swing.JButton btMenu;
    private javax.swing.JButton btModificarGLPI;
    private javax.swing.JButton btNuevoEquipo;
    private javax.swing.JButton btNuevoMaterial;
    private javax.swing.JButton btRegistrar;
    private javax.swing.JButton btSalidaMaterial;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private com.toedter.calendar.JDateChooser jdcFechaMovimiento;
    private javax.swing.JFormattedTextField jftxtCodAct;
    private javax.swing.JFormattedTextField jftxtNumero;
    private javax.swing.JFormattedTextField jftxtRAM;
    private javax.swing.JPanel jpEquipoSoftware;
    private javax.swing.JPanel jpGLPI;
    private javax.swing.JPanel jpHardware;
    private javax.swing.JPanel jpLogin;
    private javax.swing.JPanel jpMateriales;
    private javax.swing.JMenuItem menuAcercaDe;
    private javax.swing.JMenuItem menuCerrarSesion;
    private javax.swing.JMenuItem menuDepartamentos;
    private javax.swing.JMenuItem menuOficinas;
    private javax.swing.JMenuItem menuReportes;
    private javax.swing.JMenuItem menuRespaldarDB;
    private javax.swing.JMenuItem menuRestaurarDB;
    private javax.swing.JMenuItem menuSalir;
    private javax.swing.JMenuItem menuSoftware;
    private javax.swing.JMenuItem menuUsuariosLab;
    private javax.swing.JMenuItem menuUsuariosPC;
    private javax.swing.JPanel panelGlobal;
    public javax.swing.JTable tabBuscarEquipos;
    public javax.swing.JTable tabBuscarEquiposSoftware;
    private javax.swing.JTable tabBuscarGLPI;
    private javax.swing.JTable tabBuscarMateriales;
    private javax.swing.JTable tabListadoEquipos;
    public javax.swing.JTable tabListadoEquiposSoftware;
    public javax.swing.JTable tabListadoGLPI;
    private javax.swing.JTable tabListadoMateriales;
    private javax.swing.JTable tabListadoMovimientos;
    private javax.swing.JPasswordField txtContraseña;
    private javax.swing.JTextField txtDescMaterial;
    private javax.swing.JLabel txtLabelUsuario;
    public javax.swing.JLabel txtMsg;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
