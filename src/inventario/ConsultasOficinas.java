/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class ConsultasOficinas extends Conexion {

    DefaultTableModel modeloTab;

    public boolean registrar(Oficina oficina) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "INSERT INTO oficina (abreviatura, nombre, zona, cusuario) VALUES (?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, oficina.getAbreviatura());
            ps.setString(2, oficina.getNombre());
            ps.setString(3, oficina.getZona());
            ps.setString(4, ventanaPrincipal.cusuario);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean registrarEquipoOficina(int idOficina, int idEquipo, String departamento) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "INSERT INTO toficina_equipos (idOficina, idEquipo, departamento, cusuario) VALUES (?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, idOficina);
            ps.setInt(2, idEquipo);
            ps.setString(3, departamento);
            ps.setString(4, ventanaPrincipal.cusuario);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean modificarEquipoOficina(int idOficina, int idEquipo, String departamento) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE toficina_equipos SET fhasta=NOW() where idEquipo=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, idEquipo);
            ps.execute();
            
            sql = "INSERT INTO toficina_equipos (idOficina, idEquipo, departamento, cusuario) VALUES (?,?,?,?)";

            ps = con.prepareStatement(sql);
            ps.setInt(1, idOficina);
            ps.setInt(2, idEquipo);
            ps.setString(3, departamento);
            ps.setString(4, ventanaPrincipal.cusuario);
            ps.execute();
            
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean modificar(Oficina oficina) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE oficina SET fhasta=NOW() where id=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, oficina.getId());
            ps.execute();

            sql = "INSERT INTO oficina (id, abreviatura, nombre, zona, cusuario) VALUES (?,?,?,?,?)";

            ps = con.prepareStatement(sql);
            ps.setInt(1, oficina.getId());
            ps.setString(2, oficina.getAbreviatura());
            ps.setString(3, oficina.getNombre());
            ps.setString(4, oficina.getZona());
            ps.setString(5, ventanaPrincipal.cusuario);
            ps.execute();

            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean caducar(Oficina oficina) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE oficina SET fhasta=NOW(), cusuario=? where id=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, ventanaPrincipal.cusuario);
            ps.setInt(2, oficina.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean eliminarEquipo(Equipo equipo) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE toficina_equipos SET fhasta=NOW() WHERE idEquipo=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, equipo.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean buscar(Oficina oficina, javax.swing.JTable tabListado) {
        tabListado.getRowSorter().setSortKeys(null);
        modeloTab = (DefaultTableModel) tabListado.getModel();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM (SELECT IFNULL(id,'') id, IFNULL(nombre,'') nombre, IFNULL(abreviatura,'') abreviatura, IFNULL(zona,'') zona FROM oficina where fhasta='2999-12-31 00:00:00') listaOficinas WHERE nombre like IFNULL(nullif(?,''),'%') and abreviatura like IFNULL(nullif(?,''),'%') and zona like IFNULL(nullif(?,''),'%') order by nombre;";
        modeloTab.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, oficina.getNombre());
            ps.setString(2, oficina.getAbreviatura());
            ps.setString(3, oficina.getZona());
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);
                modeloTab.setValueAt(rs.getInt("id"), i, 0);
                modeloTab.setValueAt(rs.getString("nombre"), i, 1);
                modeloTab.setValueAt(rs.getString("abreviatura"), i, 2);
                modeloTab.setValueAt(rs.getString("zona"), i, 3);
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public int buscarIdOficina(Oficina oficina) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT oficina.id FROM oficina WHERE nombre=?";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, oficina.getNombre());
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                pos = rs.getInt("id");
            }
            return pos;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return pos;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public int existeEquipoOficina(int idEquipo) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT count(idEquipo) FROM toficina_equipos WHERE idEquipo=? and fhasta='2999-12-31 00:00:00'";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, idEquipo);
            rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }

            return 1;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return 1;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    public int existeDepartamentoOficina(int idEquipo, int idOficina, String departamento) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT count(idEquipo) FROM toficina_equipos WHERE idEquipo=? and idOficina=? and departamento=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, idEquipo);
            ps.setInt(2, idOficina);
            ps.setString(3, departamento);
            rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }

            return 1;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return 1;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean listar(javax.swing.JTable tabListadoOficina, DefaultTableModel modeloTabOficina) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM oficina";
        modeloTabOficina.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTabOficina.addRow(new Object[i]);
                for (int j = 0; j < 4; j++) {
                    tabListadoOficina.setValueAt(rs.getString(j + 1), i, j);
                }
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean buscarEquipos(Oficina oficina, Equipo equipo, javax.swing.JTable tabListado, DefaultTableModel modeloTab) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "select equiposoficina.idEquipo,equiposoficina.codActivo, equiposoficina.tipo, equiposoficina.marca, equiposoficina.idOficina, equiposoficina.nombre, equiposoficina.departamento from (SELECT equipo.idEquipo,equipo.codActivo, equipo.tipo, equipo.marca, ofi.idOficina, ofi.nombre, ofi.departamento from ( SELECT oficina.id idOficina, oficina.nombre, oficina.zona, Toficina_Equipos.departamento, Toficina_Equipos.idEquipo FROM Toficina_Equipos, oficina WHERE Toficina_Equipos.idOficina = oficina.id ) ofi right JOIN equipo ON ofi.idEquipo = equipo.idEquipo) equiposoficina where equiposoficina.codActivo like IFNULL(nullif(?,''),'%') and equiposoficina.tipo like IFNULL(nullif(?,''),'%') and equiposoficina.marca like IFNULL(nullif(?,''),'%') and equiposoficina.nombre like nullif(?,'') or nullif(?,'') is null";
        modeloTab.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, equipo.getCodActivo());
            ps.setString(2, equipo.getTipo());
            ps.setString(3, equipo.getMarca());
            ps.setString(4, oficina.getNombre());
            ps.setString(5, oficina.getNombre());
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);
                for (int j = 0; j < 7; j++) {
                    tabListado.setValueAt(rs.getString(j + 1), i, j);
                }
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean buscarEquipos2(Equipo equipo, Oficina oficina, Departamento departamento, String usuarioPC, String fecha, javax.swing.JTable tabListado) {
        tabListado.getRowSorter().setSortKeys(null);
        modeloTab = (DefaultTableModel) tabListado.getModel();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "select thardware.*, IfNull(tes.usuarioPC,'') usuarioPC from (select * from (SELECT equipo.idEquipo, ifnull(equipo.codActivo,'') codActivo, ifnull(equipo.tipo,'') tipo, ifnull(equipo.marca,'') marca, ifnull(equipo.modelo,'') modelo, ifnull(equipo.serial,'') serial,ifnull(equipo.procesador,'') procesador,ifnull(equipo.ram,'') ram,ifnull(equipo.discoduro,'') discoduro, ifnull(equipo.nombre,'') nombre,ifnull(equipo.mac,'') mac,ifnull(equipo.ip,'') ip,ifnull(equipo.estado,'') estado, ifnull(equipo.observacion,'') observacion, ifnull(equipo.ubicacion,'') ubicacion, DATE(equipo.fdesde) fecha, IfNull(ofi.idOficina,'') idOficina, IfNull(ofi.nombreOfi,'') nombreOfi, IfNull(ofi.departamento,'') departamento from ( SELECT oficina.id idOficina, oficina.nombre nombreOfi, toficina_equipos.departamento, toficina_equipos.idEquipo FROM toficina_equipos, oficina WHERE toficina_equipos.idOficina = oficina.id and oficina.fhasta='2999-12-31 00:00:00' and toficina_equipos.fhasta='2999-12-31 00:00:00') ofi right JOIN equipo ON ofi.idEquipo = equipo.idEquipo where equipo.fhasta='2999-12-31 00:00:00') equiposoficina where codActivo like IFNULL(nullif(?,''),'%') and tipo like IFNULL(nullif(?,''),'%') and marca like IFNULL(nullif(?,''),'%') and modelo like IFNULL(nullif(?,''),'%') and serial like IFNULL(nullif(?,''),'%') and procesador like IFNULL(nullif(?,''),'%') and ram like IFNULL(nullif(?,''),'%') and discoduro like IFNULL(nullif(?,''),'%') and nombre like IFNULL(nullif(?,''),'%') and mac like IFNULL(nullif(?,''),'%') and ip like IFNULL(nullif(?,''),'%') and estado like IFNULL(nullif(?,''),'%') and fecha like IFNULL(nullif(?,''),'%') and observacion like IFNULL(nullif(?,''),'%') and ubicacion like IFNULL(nullif(?,''),'%') and equiposoficina.nombreOfi like IFNULL(nullif(?,''),'%') and equiposoficina.departamento like IFNULL(nullif(?,''),'%')) thardware left join (select idEquipo, usuarioPC from tequipos_software where fhasta='2999-12-31 00:00:00' group by idEquipo, usuarioPC) tes on thardware.idEquipo=tes.idEquipo WHERE IfNull(tes.usuarioPC,'') like IFNULL(nullif(?,''),'%');";
        modeloTab.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, equipo.getCodActivo());
            ps.setString(2, equipo.getTipo());
            ps.setString(3, equipo.getMarca());
            ps.setString(4, equipo.getModelo());
            ps.setString(5, equipo.getSerial());
            ps.setString(6, equipo.getProcesador());
            ps.setInt(7, equipo.getRam());
            ps.setInt(8, equipo.getDiscoduro());
            ps.setString(9, equipo.getNombre());
            ps.setString(10, equipo.getMac());
            ps.setString(11, equipo.getIp());
            ps.setString(12, equipo.getEstado());
            ps.setString(13, fecha);
            ps.setString(14, equipo.getObservacion());
            ps.setString(15, equipo.getUbicacion());

            ps.setString(16, oficina.getNombre());
            ps.setString(17, departamento.getNombre());
            
            ps.setString(18, usuarioPC);
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);
                tabListado.setValueAt(rs.getInt(1), i, 0);
                for (int j = 1; j < 10; j++) {
                    tabListado.setValueAt(rs.getString(j + 1), i, j);
                }
                tabListado.setValueAt(rs.getString("usuarioPC"), i, 10);
                for (int j = 11; j < 15; j++) {
                    tabListado.setValueAt(rs.getString(j), i, j);
                }
                
                Oficina ofi=new Oficina();
                ofi.setId(rs.getInt("idOficina"));
                ofi.setNombre(rs.getString("nombreOfi"));
                tabListado.setValueAt(rs.getInt("idOficina"), i, 15);
                tabListado.setValueAt(ofi, i, 16);
                
                Departamento depar=new Departamento();
                depar.setNombre(rs.getString("departamento"));
                tabListado.setValueAt(depar, i, 17);
                
                tabListado.setValueAt(rs.getString("ubicacion"), i, 18);
                tabListado.setValueAt(rs.getString("fecha"), i, 19);
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public JComboBox llenarCombBox() {
        JComboBox<Oficina> comboBoxOficina = new JComboBox();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM oficina where fhasta='2999-12-31 00:00:00' order by nombre;";
        try {

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                Oficina itemOficina = new Oficina();
                itemOficina.setId(rs.getInt("id"));
                itemOficina.setNombre(rs.getString("nombre"));
                comboBoxOficina.addItem(itemOficina);
            }
            return comboBoxOficina;

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return comboBoxOficina;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    public JComboBox llenarComboBoxZona() {
        JComboBox comboBox = new JComboBox();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT zona FROM oficina WHERE fhasta='2999-12-31 00:00:00' GROUP BY zona";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                comboBox.addItem(rs.getString(1));
            }
            return comboBox;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return comboBox;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

}
