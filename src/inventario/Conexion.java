/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Marco Gutama
 */
public class Conexion {

    private final String driver = "com.mysql.jdbc.Driver";
    private final String user = "root";
    private final String pass = "labJA2020";
    private final String url_produccion = "jdbc:mysql://172.18.10.57:3306/db_inventario"; //Produccion
    private final String url_desarrollo = "jdbc:mysql://localhost:3306/prueba_respaldo"; //Desarrollo
    private Connection con = null;
    private final String ambiente = "produccion";

    public Connection getConexion() {

        try {
            Class.forName(driver);
            // Nos conectamos a la bd
            if(ambiente.equals("produccion")){
                System.out.println("Ambiente produccion");
                con = (Connection) DriverManager.getConnection(this.url_produccion, this.user, this.pass);               
            }else{
                System.out.println("Ambiente desarrollo");
                con = (Connection) DriverManager.getConnection(this.url_desarrollo, this.user, this.pass);
            }
            

        } catch (SQLException e) {
            System.err.println(e);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }

        return con;
    }

}
