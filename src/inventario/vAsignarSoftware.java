/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Marco Gutama
 */
public class vAsignarSoftware extends javax.swing.JDialog {

    DefaultTableModel modeloTabSoftware;
    public ConsultasSoftware conSoft;
    public ConsultasUsuariosPC conUsuarioPC;
    public UsuarioPC usuarioPCantiguo;
    public UsuarioPC usuarioPCnuevo;
    int contFilas;
    public Equipo equipo;
    JTextField txtUsuario;
    List<ControlTabla> registroTabla;

    /**
     * Creates new form vSoftware2
     */
    public vAsignarSoftware(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        this.setModalityType(Dialog.ModalityType.MODELESS);
        modeloTabSoftware = (DefaultTableModel) tabSoftware.getModel();
        contFilas = 0;

        conUsuarioPC = new ConsultasUsuariosPC();
        usuarioPCnuevo = new UsuarioPC();
        tabSoftware.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        ///////
        crearComboBox();
        ///////
        registroTabla = new ArrayList<>();
        Action action = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                TableCellListener tcl = (TableCellListener) e.getSource();
                System.out.println("Row   : " + tcl.getRow());
                System.out.println("Column: " + tcl.getColumn());
                System.out.println("Old   : " + tcl.getOldValue());
                System.out.println("New   : " + tcl.getNewValue());

                controlCambiosTabla(tcl, registroTabla);
            }
        };
        TableCellListener tcl = new TableCellListener(tabSoftware, action);
    }

    public void controlCambiosTabla(TableCellListener tcl, List<ControlTabla> registroTablaEquipos) {
        ControlTabla controlTabla = new ControlTabla(tcl.getRow(), tcl.getColumn(), "" + tcl.getOldValue(), "" + tcl.getNewValue());
        registroTablaEquipos.add(controlTabla);
    }

    public void crearComboBox() {
        JPanel jpComboBox = new JPanel();
        jpComboBox.setLayout(new FlowLayout(
                SwingConstants.LEADING, 5, 1));
        txtUsuario = new JTextField(10);
        txtUsuario.setEditable(false);
        jpComboBox.add(txtUsuario);

        JButton south = new BasicArrowButton(BasicArrowButton.SOUTH, null, null, Color.BLACK, null);
        jpComboBox.add(south);

        // WARNING:  Not sensitive to PLAF change!
        // jpComboBox.setBackground(txtUsuario.getBackground());
        jpComboBox.setBackground(Color.white);
        // jpComboBox.setBorder(txtUsuario.getBorder());
        txtUsuario.setBorder(null);
        // END WARNING:  

        jpPanelComboBox.setLayout(new GridBagLayout());
        jpPanelComboBox.add(jpComboBox);

        south.setBackground(Color.WHITE);
        //south.setBorder(boton.getBorder());

        south.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ArrowButtonPressed();
            }
        });
    }

    public void ArrowButtonPressed() {
        int posX = jpPanelComboBox.getLocationOnScreen().x;
        int posY = jpPanelComboBox.getLocationOnScreen().y;
        vListaUsuarios ventana;// = new vListaUsuarios(null, true);
        ventana = new vListaUsuarios(null, true);
        ventana.setLocation(posX, posY + 20);
        ventana.ventanaSoft = this;
        ventana.setVisible(true);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabSoftware = new javax.swing.JTable();
        btAceptar = new javax.swing.JButton();
        btCancelar = new javax.swing.JButton();
        txtCodActivo = new javax.swing.JLabel();
        btUsuario = new javax.swing.JButton();
        btAgregarSoftware = new javax.swing.JButton();
        btEliminarSoftware = new javax.swing.JButton();
        btBorrarUsuario = new javax.swing.JButton();
        jpPanelComboBox = new javax.swing.JPanel();
        btSoftware = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Asignación de software");

        jLabel1.setText("Cod. activo:");

        jLabel2.setText("Usuario PC:");

        jLabel3.setText("Software:");

        tabSoftware.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Nombre", "Versión", "Tipo", "Licencia/Key Activación", "Instalado"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabSoftware);
        if (tabSoftware.getColumnModel().getColumnCount() > 0) {
            tabSoftware.getColumnModel().getColumn(0).setMinWidth(0);
            tabSoftware.getColumnModel().getColumn(0).setPreferredWidth(0);
            tabSoftware.getColumnModel().getColumn(0).setMaxWidth(0);
            tabSoftware.getColumnModel().getColumn(1).setPreferredWidth(30);
            tabSoftware.getColumnModel().getColumn(2).setPreferredWidth(20);
            tabSoftware.getColumnModel().getColumn(3).setPreferredWidth(30);
            tabSoftware.getColumnModel().getColumn(5).setMinWidth(0);
            tabSoftware.getColumnModel().getColumn(5).setPreferredWidth(0);
            tabSoftware.getColumnModel().getColumn(5).setMaxWidth(0);
        }

        btAceptar.setText("Aceptar");
        btAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAceptarActionPerformed(evt);
            }
        });

        btCancelar.setText("Cancelar");
        btCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCancelarActionPerformed(evt);
            }
        });

        btUsuario.setText("Ξ");
        btUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUsuarioActionPerformed(evt);
            }
        });

        btAgregarSoftware.setText("+");
        btAgregarSoftware.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAgregarSoftwareActionPerformed(evt);
            }
        });

        btEliminarSoftware.setText("-");
        btEliminarSoftware.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btEliminarSoftwareActionPerformed(evt);
            }
        });

        btBorrarUsuario.setText("-");
        btBorrarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btBorrarUsuarioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpPanelComboBoxLayout = new javax.swing.GroupLayout(jpPanelComboBox);
        jpPanelComboBox.setLayout(jpPanelComboBoxLayout);
        jpPanelComboBoxLayout.setHorizontalGroup(
            jpPanelComboBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 137, Short.MAX_VALUE)
        );
        jpPanelComboBoxLayout.setVerticalGroup(
            jpPanelComboBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 23, Short.MAX_VALUE)
        );

        btSoftware.setText("Ξ");
        btSoftware.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSoftwareActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jpPanelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(17, 17, 17)
                                .addComponent(btBorrarUsuario)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btUsuario))
                            .addComponent(txtCodActivo, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btAgregarSoftware)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btEliminarSoftware)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btSoftware)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(134, 134, 134)
                .addComponent(btAceptar)
                .addGap(80, 80, 80)
                .addComponent(btCancelar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txtCodActivo, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jLabel2)
                            .addComponent(btBorrarUsuario)
                            .addComponent(btUsuario)
                            .addComponent(jpPanelComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jLabel3)
                            .addComponent(btAgregarSoftware)
                            .addComponent(btEliminarSoftware)
                            .addComponent(btSoftware))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btAceptar)
                            .addComponent(btCancelar))
                        .addContainerGap())))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCancelarActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btCancelarActionPerformed

    private void btAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAceptarActionPerformed
        // TODO add your handling code here: 
        ControlTabla[] c = new ControlTabla[registroTabla.size()];
        for (int i = 0; i < registroTabla.size(); i++) {
            c[i] = registroTabla.get(i);
        }
        registroTabla.clear();

        System.out.println("Object array before sorting : " + Arrays.toString(c));
        Arrays.sort(c, new ControlTabla.FilaColumnaComparator());
        System.out.println("Object array after sorting in natural order : " + Arrays.toString(c));

        if (java.util.Objects.equals(usuarioPCantiguo.getUsuario(), txtUsuario.getText()) || (usuarioPCantiguo.getUsuario() == null && txtUsuario.getText().equals(""))) {//Mismo usuario
            for (int i = 0; i < tabSoftware.getRowCount(); i++) {//Registramos solo software nuevo
                if (!(boolean) tabSoftware.getValueAt(i, 5)) {//Si no esta instalado, entonces registra
                    conUsuarioPC.registrarEquipoSoftware(equipo.getId(), usuarioPCantiguo.getUsuario(), (int) tabSoftware.getValueAt(i, 0), (String) tabSoftware.getValueAt(i, 4));
                }
            }
            if (c.length > 0) {//Si se ha modificado licencia
                controlModificacionLicencia(c);
            }
        } else {//Entonces es usuario nuevo

            conUsuarioPC.desasignarSoftware(equipo.getId(), usuarioPCantiguo.getUsuario()); //Desasignamos software suario anterior
            if (tabSoftware.getRowCount() > 0) { //Consultamos si hay softaware para asignar
                for (int i = 0; i < tabSoftware.getRowCount(); i++) {//Registramos todo el software
                    conUsuarioPC.registrarEquipoSoftware(equipo.getId(), usuarioPCnuevo.getUsuario(), (int) tabSoftware.getValueAt(i, 0), (String) tabSoftware.getValueAt(i, 4));
                }
            }else{
                conUsuarioPC.registrarEquipoSoftware(equipo.getId(), usuarioPCnuevo.getUsuario(), null, null);
            }

        }
        this.dispose();
    }//GEN-LAST:event_btAceptarActionPerformed

    public void controlModificacionLicencia(ControlTabla[] c) {
        for (int i = 0; i < c.length; i++) {
            if ((boolean) tabSoftware.getValueAt(c[i].fila, 5)) { //Si es una programa que ya ha estado isntalado
                conUsuarioPC.desasignarSoftwareUsuario(equipo.getId(), usuarioPCantiguo.getUsuario(), (int) tabSoftware.getValueAt(c[i].fila, 0)); //Desasignamos software suario anterior
                conUsuarioPC.registrarEquipoSoftware(equipo.getId(), usuarioPCantiguo.getUsuario(), (int) tabSoftware.getValueAt(c[i].fila, 0), (String) tabSoftware.getValueAt(c[i].fila, 4));
            }
        }

    }

    private void btUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUsuarioActionPerformed
        // TODO add your handling code here:
        vUsuariosPC ventana = new vUsuariosPC(null, true);
        ventana.ventanaSoft = this;
        ventana.setVisible(true);

    }//GEN-LAST:event_btUsuarioActionPerformed

    private void btAgregarSoftwareActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAgregarSoftwareActionPerformed
        // TODO add your handling code here:
        int posX = btAgregarSoftware.getLocationOnScreen().x;
        int posY = btAgregarSoftware.getLocationOnScreen().y;
        vListaSoftware ventana = new vListaSoftware(null, true);
        ventana.setLocation(posX, posY + 30);
        ventana.ventanaSoft = this;
        //ventana.validaDatosSeleccionados();
        //ventana.setLocationRelativeTo(this);
        ventana.setVisible(true);
    }//GEN-LAST:event_btAgregarSoftwareActionPerformed

    private void btEliminarSoftwareActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btEliminarSoftwareActionPerformed
        // TODO add your handling code here:
        int pos = tabSoftware.getSelectedRow();
        if (pos >= 0) {
            if ((boolean) tabSoftware.getValueAt(pos, 5)) {
                int a = JOptionPane.showConfirmDialog(null, "Desea desinstalar el programa " + (String) tabSoftware.getValueAt(pos, 2));
                if (a == JOptionPane.YES_OPTION) {
                    if (conUsuarioPC.desinstalarSoftware(equipo.getId(), (int) tabSoftware.getValueAt(pos, 0))) {
                        System.out.println("Software desinstalado");
                        modeloTabSoftware.removeRow(pos);
                    }

                }

            } else {
                modeloTabSoftware.removeRow(pos);
            }
        }
    }//GEN-LAST:event_btEliminarSoftwareActionPerformed

    private void btBorrarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btBorrarUsuarioActionPerformed
        // TODO add your handling code here:
        txtUsuario.setText("");
        usuarioPCnuevo.setId(0);
        usuarioPCnuevo.setUsuario(null);
    }//GEN-LAST:event_btBorrarUsuarioActionPerformed

    private void btSoftwareActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSoftwareActionPerformed
        // TODO add your handling code here:
        vSoftware ventana = new vSoftware(null, true);
        ventana.setVisible(true);
    }//GEN-LAST:event_btSoftwareActionPerformed

    public void cargarDatosUsuario(UsuarioPC usuario) {
        usuarioPCnuevo = usuario;
        //cbUsuarioPC.setText(usuarioVLista.getUsuarioId());
        txtUsuario.setText(usuario.getUsuario());
        //txtNombreUsuario.setText(usuarioVLista.getNombre() + " " + usuarioVLista.getApellido());
    }

    public void cargarDatosSoftware(Software sofList) {
        modeloTabSoftware.addRow(new Object[0]);
        int pos = tabSoftware.getRowCount() - 1;
        tabSoftware.setValueAt(sofList.getId(), pos, 0);
        tabSoftware.setValueAt(sofList.getNombre(), pos, 1);
        tabSoftware.setValueAt(sofList.getVersion(), pos, 2);
        tabSoftware.setValueAt(sofList.getTipo(), pos, 3);
        tabSoftware.setValueAt(false, pos, 5);//Para saber que aun no esta insatalado
        contFilas++;
    }

    public void cargarSoftwareInstalado(Equipo equipo) {
        usuarioPCantiguo = conUsuarioPC.listarSoftwareInstalado(equipo, tabSoftware);
        if (usuarioPCantiguo.getUsuario() != null) {
            txtUsuario.setText(usuarioPCantiguo.getUsuario());
            // txtNombreUsuario.setText(usuarioPCantiguo.getNombre() + " " + usuarioPCantiguo.getApellido());
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(vAsignarSoftware.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(vAsignarSoftware.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(vAsignarSoftware.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(vAsignarSoftware.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                vAsignarSoftware dialog = new vAsignarSoftware(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAceptar;
    private javax.swing.JButton btAgregarSoftware;
    private javax.swing.JButton btBorrarUsuario;
    private javax.swing.JButton btCancelar;
    private javax.swing.JButton btEliminarSoftware;
    private javax.swing.JButton btSoftware;
    private javax.swing.JButton btUsuario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel jpPanelComboBox;
    public javax.swing.JTable tabSoftware;
    public javax.swing.JLabel txtCodActivo;
    // End of variables declaration//GEN-END:variables
}
