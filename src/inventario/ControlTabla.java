/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.util.Comparator;

/**
 *
 * @author Marco Gutama
 */
class ControlTabla implements Comparable<ControlTabla> {

    int fila;
    int columna;
    String valorAntiguo;
    String valorNuevo;

    public ControlTabla(int fila, int columna, String valorAntiguo, String valorNuevo) {
        this.fila = fila;
        this.columna = columna;
        this.valorAntiguo = valorAntiguo;
        this.valorNuevo = valorNuevo;
    }

    public String toString() {
        return ("(" + fila + ", " + columna + ", " + valorAntiguo + ", " + valorNuevo + ")");
    }

    @Override
    public int compareTo(ControlTabla o) {
        // usually toString should not be used,
        // instead one of the attributes or more in a comparator chain
        return toString().compareTo(o.toString());
    }

    public static class FilaColumnaComparator implements Comparator<ControlTabla> {

        @Override
        public int compare(ControlTabla o1, ControlTabla o2) {
            int comFila = o1.fila - o2.fila;
            if (comFila == 0) {//Las filas son iguales, ordenamos por columnas
                return o1.columna - o2.columna;
            } else {
                return comFila;
            }
        }

    }

}
