/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class ConsultasDepartamentos extends Conexion {

    DefaultTableModel modeloTab;

    public boolean registrar(Departamento d) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "INSERT INTO tdepartamentos (nombre, abreviatura, cusuario) VALUES (?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, d.getNombre());
            ps.setString(2, d.getAbreviatura());
            ps.setString(3, ventanaPrincipal.cusuario);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean modificar(Departamento d) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE tdepartamentos SET fhasta=NOW() where id=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, d.getId());
            ps.execute();

            sql = "INSERT INTO tdepartamentos (id, nombre, abreviatura, cusuario) VALUES (?,?,?,?)";

            ps = con.prepareStatement(sql);
            ps.setInt(1, d.getId());
            ps.setString(2, d.getNombre());
            ps.setString(3, d.getAbreviatura());
            ps.setString(4, ventanaPrincipal.cusuario);
            ps.execute();

            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean actualizar(Departamento d) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE tdepartamentos SET nombre=?, abreviatura=?, cusuario=?, fdesde=CURRENT_TIMESTAMP() WHERE id=?";
        try {
            ps = con.prepareStatement(sql);           
            ps.setString(1, d.getNombre());
            ps.setString(2, d.getAbreviatura());
            ps.setString(3, ventanaPrincipal.cusuario);
            ps.setInt(4, d.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }

    }

    public boolean caducar(Departamento d) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE tdepartamentos SET fhasta=NOW() where id=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, d.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean buscar(Departamento d, javax.swing.JTable tabListado) {
        tabListado.getRowSorter().setSortKeys(null);
        modeloTab = (DefaultTableModel) tabListado.getModel();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM (SELECT id, nombre, IFNULL(abreviatura,'') abreviatura FROM tdepartamentos where fhasta='2999-12-31 00:00:00') listaDepartamentos WHERE nombre like IFNULL(nullif(?,''),'%') and abreviatura like IFNULL(nullif(?,''),'%') order by nombre;";
        modeloTab.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, d.getNombre());
            ps.setString(2, d.getAbreviatura());
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);
                modeloTab.setValueAt(rs.getInt("id"), i, 0);
                modeloTab.setValueAt(rs.getString("nombre"), i, 1);
                modeloTab.setValueAt(rs.getString("abreviatura"), i, 2);
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public int buscarId(Departamento d) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT id FROM tdepartamentos WHERE nombre=? and fhasta='2999-12-31 00:00:00'";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, d.getNombre());
            rs = ps.executeQuery();

            int i = 0;
            if (rs.next()) {
                pos = rs.getInt("id");
            }
            return pos;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return pos;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public JComboBox llenarCombBox() {
        JComboBox<Departamento> comboBox = new JComboBox();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM tdepartamentos where fhasta='2999-12-31 00:00:00' order by nombre;";
        try {

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                Departamento item = new Departamento();
                item.setId(rs.getInt("id"));
                item.setNombre(rs.getString("nombre"));
                comboBox.addItem(item);
            }
            return comboBox;

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return comboBox;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    public int existeOtroDepartamento(Departamento departamento) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT count(id) FROM tdepartamentos WHERE nombre=? and fhasta='2999-12-31 00:00:00' and id<>?";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, departamento.getNombre());
            ps.setInt(2, departamento.getId());
            rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }

            return 1;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return 1;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

}
