/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class ConsultasGLPI extends Conexion {

    DefaultTableModel modeloTab;

    public boolean registrar(GLPI glpi) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "INSERT INTO glpi (idEquipo, fecha, num_glpi, descripcion, cusuario) VALUES (?,?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, glpi.getIdEquipo());
            ps.setString(2, glpi.getFecha());
            ps.setString(3, glpi.getNum_glpi());
            ps.setString(4, glpi.getDescripcion());
            ps.setString(5, ventanaPrincipal.cusuario);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
        public boolean modificar(GLPI glpi) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE glpi SET fhasta=NOW() where id=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, glpi.getId());
            ps.execute();

            sql = "INSERT INTO glpi (id, idEquipo, fecha, num_glpi, descripcion, cusuario) VALUES (?,?,?,?,?,?)";

            ps = con.prepareStatement(sql);
            ps.setInt(1, glpi.getId());
            ps.setInt(2, glpi.getIdEquipo());
            ps.setString(3, glpi.getFecha());
            ps.setString(4, glpi.getNum_glpi());
            ps.setString(5, glpi.getDescripcion());
            ps.setString(6, ventanaPrincipal.cusuario);
            ps.execute();

            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean eliminar(GLPI glpi) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE glpi SET fhasta=NOW() where id=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, glpi.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean buscar(GLPI glpi) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM glpi WHERE id=? and fhasta='2999-12-31 00:00:00';";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, glpi.getId());
            rs = ps.executeQuery();

            while (rs.next()) {
                glpi.setIdEquipo(rs.getInt("idEquipo"));
                glpi.setFecha(rs.getString("fecha"));
                glpi.setNum_glpi(rs.getString("num_glpi"));
                glpi.setDescripcion(rs.getString("descripcion"));
                glpi.setTecnico(rs.getString("cusuario"));
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean buscarEquipoGLpi(Equipo equipo, GLPI glpi, javax.swing.JTable tabListado) {
        tabListado.getRowSorter().setSortKeys(null);
        modeloTab = (DefaultTableModel) tabListado.getModel();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "select * from (select ifnull(equipo.codActivo,'') codActivo, ifnull(equipo.tipo,'') tipo, glpi.* from (SELECT * FROM equipo WHERE fhasta IN ( SELECT MAX(fhasta) FROM equipo GROUP BY idEquipo )) equipo, glpi where equipo.idEquipo=glpi.idEquipo and glpi.fhasta='2999-12-31 00:00:00' GROUP BY glpi.id) listGlpi where listGlpi.fecha like IFNULL(nullif(?,''),'%') and listGlpi.codActivo like IFNULL(nullif(?,''),'%') and listGlpi.tipo like IFNULL(nullif(?,''),'%') and listGlpi.cusuario like IFNULL(nullif(?,''),'%') and listGlpi.num_glpi like IFNULL(nullif(?,''),'%') and listGlpi.descripcion like IFNULL(nullif(?,''),'%') ORDER BY fdesde DESC;";
        modeloTab.setRowCount(0);

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, glpi.getFecha());
            ps.setString(2, equipo.getCodActivo());
            ps.setString(3, equipo.getTipo());
            ps.setString(4, glpi.getTecnico());
            ps.setString(5, glpi.getNum_glpi());
            ps.setString(6, glpi.getDescripcion());
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);
                tabListado.setValueAt(rs.getInt("id"), i, 0);
                tabListado.setValueAt(rs.getInt("idEquipo"), i, 1);
                tabListado.setValueAt(rs.getString("fecha"), i, 2);
                tabListado.setValueAt(rs.getString("codActivo"), i, 3);
                tabListado.setValueAt(rs.getString("tipo"), i, 4);
                tabListado.setValueAt(rs.getString("cusuario"), i, 5);
                tabListado.setValueAt(rs.getString("num_glpi"), i, 6);
                tabListado.setValueAt(rs.getString("descripcion"), i, 7);
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

}
