/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuariopc
 */
public class ConsultasUsuariosPC extends Conexion {

    DefaultTableModel modeloTab;

    public boolean registrar(UsuarioPC usuario) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "INSERT INTO tusuarios_pc (usuario, nombre, apellido, cusuario) VALUES (?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getNombre());
            ps.setString(3, usuario.getApellido());
            ps.setString(4, ventanaPrincipal.cusuario);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }

    public boolean modificar(UsuarioPC usuario) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        if (caducar(usuario)) {
            String sql = "INSERT INTO tusuarios_pc (id, usuario, nombre, apellido, cusuario) VALUES (?,?,?,?,?)";
            try {
                ps = con.prepareStatement(sql);
                ps.setInt(1, usuario.getId());
                ps.setString(2, usuario.getUsuario());
                ps.setString(3, usuario.getNombre());
                ps.setString(4, usuario.getApellido());
                ps.setString(5, ventanaPrincipal.cusuario);
                ps.execute();
                return true;
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
                return false;
            } finally {
                try {
                    con.close();
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(null, "Error: " + e);
                }
            }
        } else {
            return false;
        }

    }

    public boolean actualizar(UsuarioPC usuario) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE tusuarios_pc SET usuario=?, nombre=?, apellido=?, cusuario=?, fdesde=CURRENT_TIMESTAMP() WHERE id=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getNombre());
            ps.setString(3, usuario.getApellido());
            ps.setString(4, ventanaPrincipal.cusuario);
            ps.setInt(5, usuario.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }

    }

    public boolean buscar(UsuarioPC usuario, javax.swing.JTable tabListado) {
        tabListado.getRowSorter().setSortKeys(null);
        modeloTab = (DefaultTableModel) tabListado.getModel();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM (SELECT IFNULL(id,'') id, IFNULL(usuario,'') usuario, IFNULL(nombre,'') nombre, IFNULL(apellido,'') apellido FROM tusuarios_pc where fhasta='2999-12-31 00:00:00') listaUsuarios WHERE usuario like IFNULL(nullif(?,''),'%') and nombre like IFNULL(nullif(?,''),'%') and apellido like IFNULL(nullif(?,''),'%') order by usuario;";
        modeloTab.setRowCount(0);

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getNombre());
            ps.setString(3, usuario.getApellido());
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);

                tabListado.setValueAt(rs.getInt("id"), i, 0);
                tabListado.setValueAt(rs.getString("usuario"), i, 1);
                tabListado.setValueAt(rs.getString("nombre"), i, 2);
                tabListado.setValueAt(rs.getString("apellido"), i, 3);

                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }

    public boolean registrarEquipoSoftware(int idEquipo, String usuarioPC, Integer idSoftware, String licencia) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "INSERT INTO tequipos_software (idEquipo, usuarioPC, idSoftware,licencia, cusuario) VALUES (?,?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, idEquipo);
            ps.setString(2, usuarioPC);
            if (idSoftware != null) {
                ps.setInt(3, idSoftware);
            } else {
                ps.setString(3, null);
            }
            ps.setString(4, licencia);
            ps.setString(5, ventanaPrincipal.cusuario);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }

    public boolean eliminarEquipoSoftware(Equipo equipo) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "DELETE FROM tequipos_software WHERE idEquipo=?";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, equipo.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }

    public boolean desinstalarSoftware(int IdEquipo, int IdSoftware) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE tequipos_software SET fhasta=NOW() where idEquipo=? and idSoftware=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, IdEquipo);
            ps.setInt(2, IdSoftware);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }

    public boolean desasignarSoftware(int IdEquipo, String usuario) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE tequipos_software SET fhasta=NOW() where idEquipo=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, IdEquipo);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }

    public boolean desasignarSoftwareUsuario(int IdEquipo, String usuario, int IdSoftware) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE tequipos_software SET fhasta=NOW() where idEquipo=? and idSoftware=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, IdEquipo);
            ps.setInt(2, IdSoftware);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }

    public boolean caducar(UsuarioPC usuario) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE tusuarios_pc SET fhasta=NOW(), cusuario=? where id=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, ventanaPrincipal.cusuario);
            ps.setInt(2, usuario.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }

    public UsuarioPC listarSoftwareInstalado(Equipo equipo, javax.swing.JTable tabListado) {
        UsuarioPC usuariopc = new UsuarioPC();
        modeloTab = (DefaultTableModel) tabListado.getModel();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        //String sql = "select equipoUsuario.idSoftware, equipoUsuario.licencia,equipoUsuario.usuarioPC, equipoUsuario.nombre,equipoUsuario.apellido, software.tipo, software.nombre nombreSoftware, software.version from (select * from(select tequipos_software.idSoftware,tequipos_software.usuarioPC,tequipos_software.licencia from tequipos_software where tequipos_software.fhasta='2999-12-31 00:00:00' and tequipos_software.idEquipo=?) equipo left join tusuarios_pc on tusuarios_pc.usuario=equipo.usuarioPC and tusuarios_pc.fhasta ='2999-12-31 00:00:00') equipoUsuario, software where equipoUsuario.idSoftware=software.id and software.fhasta ='2999-12-31 00:00:00';";
        String sql = "select equipoUsuario.idSoftware, equipoUsuario.licencia,equipoUsuario.usuarioPC, equipoUsuario.nombre,equipoUsuario.apellido, software.tipo, software.nombre nombreSoftware, software.version from (select * from(select tequipos_software.idSoftware,tequipos_software.usuarioPC,tequipos_software.licencia from tequipos_software where tequipos_software.fhasta='2999-12-31 00:00:00' and tequipos_software.idEquipo=?) equipo left join tusuarios_pc on tusuarios_pc.usuario=equipo.usuarioPC and tusuarios_pc.fhasta ='2999-12-31 00:00:00') equipoUsuario left join software on (equipoUsuario.idSoftware=software.id and software.fhasta ='2999-12-31 00:00:00');";
        modeloTab.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, equipo.getId());
            rs = ps.executeQuery();

            int i = 0;
            boolean controlUsuario = true;
            while (rs.next()) {
                if (controlUsuario) {
                    //usuariopc.setId(rs.getInt("idUsuario"));
                    usuariopc.setUsuario(rs.getString("usuarioPC"));
                    usuariopc.setNombre(rs.getString("nombre"));
                    usuariopc.setApellido(rs.getString("apellido"));
                    controlUsuario = false;
                }
                if (rs.getInt("idSoftware") != 0) {
                    modeloTab.addRow(new Object[i]);

                    tabListado.setValueAt(rs.getInt("idSoftware"), i, 0);
                    tabListado.setValueAt(rs.getString("nombreSoftware"), i, 1);
                    tabListado.setValueAt(rs.getString("version"), i, 2);
                    tabListado.setValueAt(rs.getString("tipo"), i, 3);
                    tabListado.setValueAt(rs.getString("licencia"), i, 4);
                    tabListado.setValueAt(true, i, 5);

                    i++;
                }
            }
            return usuariopc;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return usuariopc;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }

    public int buscarId(String nombreUsuario) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT id FROM tusuarios_pc WHERE usuario=?";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, nombreUsuario);
            rs = ps.executeQuery();

            if (rs.next()) {
                pos = rs.getInt("id");
            }
            return pos;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return pos;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public int existeUsuario(UsuarioPC usuario) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT count(id) FROM tusuarios_pc WHERE usuario=? and fhasta='2999-12-31 00:00:00'";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getUsuario());
            rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }

            return 1;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return 1;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public int existeUsuarioModificar(UsuarioPC usuario) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT count(id) FROM tusuarios_pc WHERE usuario=? and fhasta='2999-12-31 00:00:00' and id<>?";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getUsuario());
            ps.setInt(2, usuario.getId());
            rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }

            return 1;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return 1;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

}
