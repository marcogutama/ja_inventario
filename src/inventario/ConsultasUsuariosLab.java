/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class ConsultasUsuariosLab extends Conexion {

    DefaultTableModel modeloTab;

    public boolean registrar(UsuarioLab usuario) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "INSERT INTO tusuarios_lab (usuario, nombre, contraseña, idZona, id_tipo, cusuario) VALUES (?,?,?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getNombre());
            ps.setString(3, usuario.getContraseña());
            ps.setInt(4, usuario.getId_zona());
            ps.setInt(5, usuario.getId_tipo());
            ps.setString(6, ventanaPrincipal.cusuario);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean login(UsuarioLab usuario) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT u.usuario, u.contraseña, u.nombre, u.idZona, z.nombre AS nombreZona, u.id_tipo, t.nombre tipoUsuario, u.fhasta FROM tusuarios_lab AS u INNER JOIN ttipo_usuario AS t ON u.id_tipo=t.id INNER JOIN tzonas AS z ON u.idZona=z.id and z.fhasta='2999-12-31 00:00:00' WHERE usuario = ? AND u.fhasta='2999-12-31 00:00:00'";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getUsuario());
            rs = ps.executeQuery();

            if (rs.next()) {
                if (usuario.getContraseña().equals(rs.getString("contraseña"))) {

                    String sqlUpdate = "UPDATE tusuarios_lab SET ultima_sesion=? WHERE usuario=?";
                    ps = con.prepareStatement(sqlUpdate);
                    ps.setString(1, usuario.getUltima_sesion());
                    ps.setString(2, rs.getString("usuario"));
                    ps.execute();

                    usuario.setNombre(rs.getString("nombre"));
                    usuario.setId_tipo(rs.getInt("id_tipo"));
                    usuario.setNombre_tipo(rs.getString("tipoUsuario"));
                    usuario.setId_zona(rs.getInt("idZona"));
                    usuario.setNombre_zona(rs.getString("nombreZona"));

                    return true;
                } else {
                    return false;
                }
            }
            return false;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            } catch (Throwable e) {
                JOptionPane.showMessageDialog(null, "No hay conexion al servidor.", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
    }

    public int existeUsuario(String usuario) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT count(usuario) FROM tusuarios_lab WHERE usuario = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }

            return 1;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return 1;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean buscar(UsuarioLab usuario,String zona, String rol, javax.swing.JTable tabListado) {
        tabListado.getRowSorter().setSortKeys(null);
        modeloTab = (DefaultTableModel) tabListado.getModel();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM (SELECT tusuarios_lab.*,tzonas.nombre zona, ttipo_usuario.nombre rol FROM tusuarios_lab, tzonas, ttipo_usuario where tusuarios_lab.id_tipo=ttipo_usuario.id and tusuarios_lab.fhasta='2999-12-31 00:00:00' and tusuarios_lab.idZona=tzonas.id and tzonas.fhasta='2999-12-31 00:00:00') listaUsuarios WHERE usuario<>'admin' and usuario like IFNULL(nullif(?,''),'%') and nombre like IFNULL(nullif(?,''),'%') and zona like IFNULL(nullif(?,''),'%') and rol like IFNULL(nullif(?,''),'%') order by usuario;";
        modeloTab.setRowCount(0);

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getNombre());
            ps.setString(3, zona);
            ps.setString(4, rol);
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);

                tabListado.setValueAt(rs.getString("usuario"), i, 0);
                tabListado.setValueAt(rs.getString("nombre"), i, 1);
                tabListado.setValueAt(rs.getString("rol"), i, 3);
                tabListado.setValueAt(rs.getString("ultima_sesion"), i, 4);
                
                Zona zonaObject=new Zona();
                zonaObject.setId(rs.getInt("idZona"));
                zonaObject.setNombre(rs.getString("zona"));
                tabListado.setValueAt(zonaObject, i, 2);

                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }
    
    public UsuarioLab datosUsuario(UsuarioLab usuario) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT nombre, contraseña, idZona, id_tipo FROM tusuarios_lab WHERE fhasta='2999-12-31 00:00:00' and usuario=?";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getUsuario());
            rs = ps.executeQuery();

            if (rs.next()) {
                usuario.setNombre(rs.getString("nombre"));
                usuario.setContraseña(rs.getString("contraseña"));
                usuario.setId_zona(rs.getInt("idZona"));
                usuario.setId_tipo(rs.getInt("id_tipo"));

            }
            return usuario;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return usuario;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }

    public boolean caducar(UsuarioLab usuario) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE tusuarios_lab SET fhasta=NOW(), cusuario=? where usuario=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, ventanaPrincipal.cusuario);
            ps.setString(2, usuario.getUsuario());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }

    public boolean modificar(UsuarioLab usuario) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        if (caducar(usuario)) {
            String sql = "INSERT INTO tusuarios_lab (usuario, nombre, contraseña, idZona, id_tipo, cusuario) VALUES (?,?,?,?,?,?)";
            try {
                ps = con.prepareStatement(sql);
                ps.setString(1, usuario.getUsuario());
                ps.setString(2, usuario.getNombre());
                ps.setString(3, usuario.getContraseña());
                ps.setInt(4, usuario.getId_zona());
                ps.setInt(5, usuario.getId_tipo());
                ps.setString(6, ventanaPrincipal.cusuario);
                ps.execute();
                return true;
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
                return false;
            } finally {
                try {
                    con.close();
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(null, "Error: " + e);
                }
            }
        } else {
            return false;
        }

    }

    public String obtenerContraseña(UsuarioLab usuario) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT contraseña FROM `tusuarios_lab` WHERE usuario=? and fhasta='2999-12-31 00:00:00';";
        String contraseña = null;

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getUsuario());
            rs = ps.executeQuery();

            if (rs.next()) {
                contraseña = rs.getString("contraseña");
            }
            return contraseña;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error: " + e);
            return contraseña;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Error: " + e);
            }
        }
    }
    
    public JComboBox llenarComboBox() {
        JComboBox<Zona> comboBoxOficina = new JComboBox();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT id, nombre FROM tzonas where fhasta='2999-12-31 00:00:00' order by nombre;";
        try {

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                Zona item = new Zona();
                item.setId(rs.getInt("id"));
                item.setNombre(rs.getString("nombre"));
                comboBoxOficina.addItem(item);
            }
            return comboBoxOficina;

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return comboBoxOficina;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

}
