/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class ConsultasMateriales extends Conexion {

    public boolean registrar(Material material) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "INSERT INTO materiales (codigo, descripcion, idZona, cusuario) VALUES (?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, material.getCodigo());
            ps.setString(2, material.getDescripcion());
            ps.setInt(3, ventanaPrincipal.czona);
            ps.setString(4, ventanaPrincipal.cusuario);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean registrarMovimiento(int idMaterial, String fecha, String movimiento, int cantidad, int antStock, String observacion) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "INSERT INTO tmovimientos (idMaterial, fecha, tipomovimiento,cantidad,stock, observacion, cusuario) VALUES (?,?,?,?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, idMaterial);
            ps.setString(2, fecha);
            ps.setString(3, movimiento);
            ps.setInt(4, cantidad);
            ps.setInt(5, antStock);
            ps.setString(6, observacion);
            ps.setString(7, ventanaPrincipal.cusuario);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean buscar(Material material, javax.swing.JTable tabListado, DefaultTableModel modeloTab) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM materiales WHERE codigo like IFNULL(nullif(?,''),'%') and descripcion like IFNULL(nullif(?,''),'%')";
        modeloTab.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, material.getCodigo());
            ps.setString(2, material.getDescripcion());
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);
                for (int j = 0; j < 3; j++) {
                    tabListado.setValueAt(rs.getString(j + 1), i, j);
                }
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean calcularStock(Material material, javax.swing.JTable tabListado, DefaultTableModel modeloTab) {
        tabListado.getRowSorter().setSortKeys(null);
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "select movivimientoMateriales.id,movivimientoMateriales.codigo,movivimientoMateriales.descripcion, movivimientoMateriales.totalEntradas,movivimientoMateriales.totalSalidas from (select * from materiales left join (select totEntradas.idMaterial, totEntradas.totalEntradas, IfNull(totSalidas.totalSalidas,0) totalSalidas from (select idMaterial,tipomovimiento, sum(cantidad) totalEntradas from tmovimientos WHERE tipomovimiento=\"Entrada\" group by idMaterial) totEntradas left join (select idMaterial,tipomovimiento, sum(cantidad) totalSalidas from tmovimientos WHERE tipomovimiento=\"Salida\" group by idMaterial) totSalidas on totEntradas.idMaterial=totSalidas.idMaterial) totalMovimientos on totalMovimientos.idMaterial=materiales.id WHERE materiales.idZona=? and materiales.fhasta='2999-12-31 00:00:00') movivimientoMateriales where movivimientoMateriales.codigo like IFNULL(nullif(?,''),'%') and movivimientoMateriales.descripcion like IFNULL(nullif(?,''),'%');";
        modeloTab.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, ventanaPrincipal.czona);
            ps.setString(2, material.getCodigo());
            ps.setString(3, material.getDescripcion());
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);

                tabListado.setValueAt(rs.getInt("id"), i, 0);
                tabListado.setValueAt(rs.getString("codigo"), i, 1);
                tabListado.setValueAt(rs.getString("descripcion"), i, 2);
                tabListado.setValueAt(rs.getInt("totalEntradas") - rs.getInt("totalSalidas"), i, 3);
                tabListado.setValueAt(rs.getInt("totalEntradas"), i, 4);
                tabListado.setValueAt(rs.getInt("totalSalidas"), i, 5);

                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean buscarMovimiento(Date fecha, String descripcionMat, javax.swing.JTable tabListado, DefaultTableModel modeloTab) {
        tabListado.getRowSorter().setSortKeys(null);
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "select tmovimientos.id, tmovimientos.fecha,materiales.descripcion, tmovimientos.stock,tmovimientos.tipomovimiento,tmovimientos.cantidad,tmovimientos.observacion from materiales,tmovimientos where materiales.id=tmovimientos.idMaterial and tmovimientos.fecha like IFNULL(nullif(?,''),'%') and materiales.descripcion like IFNULL(nullif(?,''),'%') and materiales.fhasta='2999-12-31 00:00:00' and materiales.idZona=? ORDER BY fecha DESC;";
        modeloTab.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            ps.setDate(1, fecha);
            ps.setString(2, descripcionMat);
            ps.setInt(3, ventanaPrincipal.czona);
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);
                for (int j = 0; j < 7; j++) {
                    tabListado.setValueAt(rs.getString(j + 1), i, j);
                }
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public int buscarId(Material material) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT id FROM materiales WHERE codigo=?";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, material.getCodigo());
            rs = ps.executeQuery();

            int i = 0;
            if (rs.next()) {
                return rs.getInt("id");
            }
            return pos;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return pos;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean caducar(Material material) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE materiales SET fhasta=NOW(), cusuario=? where id=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, ventanaPrincipal.cusuario);
            ps.setInt(2, material.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean modificar(Material material) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        if (caducar(material)) {
            String sql = "INSERT INTO materiales (id, codigo, descripcion, idZona, cusuario) VALUES (?,?,?,?,?)";
            try {
                ps = con.prepareStatement(sql);
                ps.setInt(1, material.getId());
                ps.setString(2, material.getCodigo());
                ps.setString(3, material.getDescripcion());
                ps.setInt(4, ventanaPrincipal.czona);
                ps.setString(5, ventanaPrincipal.cusuario);
                ps.execute();
                return true;
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            } finally {
                try {
                    con.close();
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            return false;
        }

    }
    
    public int existeOtroMaterial(Material material) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT count(id) FROM materiales WHERE codigo=? and fhasta='2999-12-31 00:00:00' and id<>?";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, material.getCodigo());
            ps.setInt(2, material.getId());
            rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }

            return 1;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return 1;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

}
