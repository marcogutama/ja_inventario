/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author usuario
 */
public class ConsultasEquipos extends Conexion {

    DefaultTableModel modeloTab;

    public int registrar(Equipo equipo) {
        PreparedStatement ps = null;
        Connection con = getConexion();
        ResultSet rs = null;

        String sql = "INSERT INTO equipo(codActivo, tipo, marca, modelo, serial, procesador, ram, discoduro, nombre, mac, ip, estado, observacion, ubicacion, cusuario) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, equipo.getCodActivo());
            ps.setString(2, equipo.getTipo());
            ps.setString(3, equipo.getMarca());
            ps.setString(4, equipo.getModelo());
            ps.setString(5, equipo.getSerial());
            ps.setString(6, equipo.getProcesador());
            ps.setObject(7, equipo.getRam(),java.sql.Types.INTEGER);
            ps.setObject(8, equipo.getDiscoduro(),java.sql.Types.INTEGER);
            ps.setString(9, equipo.getNombre());
            ps.setString(10, equipo.getMac());
            ps.setString(11, equipo.getIp());
            ps.setString(12, equipo.getEstado());
            ps.setString(13, equipo.getObservacion());
            ps.setString(14, equipo.getUbicacion());
            ps.setString(15, ventanaPrincipal.cusuario);
            ps.execute();
            
            sql= "select LAST_INSERT_ID();";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            int i = 0;
            while (rs.next()) {
                pos = rs.getInt(1);
            }
            return pos;
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return pos;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean modificar(Equipo equipo) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE equipo SET fhasta=NOW() where idEquipo=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, equipo.getId());
            ps.execute();

            sql = "INSERT INTO equipo(idEquipo, codActivo, tipo, marca, modelo, serial, procesador, ram, discoduro, nombre, mac, ip, estado, observacion, ubicacion, cusuario) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = con.prepareStatement(sql);
            ps.setInt(1, equipo.getId());
            ps.setString(2, equipo.getCodActivo());
            ps.setString(3, equipo.getTipo());
            ps.setString(4, equipo.getMarca());
            ps.setString(5, equipo.getModelo());
            ps.setString(6, equipo.getSerial());
            ps.setString(7, equipo.getProcesador());
            ps.setObject(8, equipo.getRam(),java.sql.Types.INTEGER);
            ps.setObject(9, equipo.getDiscoduro(),java.sql.Types.INTEGER);
            ps.setString(10, equipo.getNombre());
            ps.setString(11, equipo.getMac());
            ps.setString(12, equipo.getIp());
            ps.setString(13, equipo.getEstado());
            ps.setString(14, equipo.getObservacion());
            ps.setString(15, equipo.getUbicacion());
            ps.setString(16, ventanaPrincipal.cusuario);
            ps.execute();

            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean modificarSoftware(String[] datos) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE tequipos_software SET usuario=?, sisOperativo=?, sisOperativo_lic=?, office=?, office_lic=?, visio=?, visio_lic=?, project=?, project_lic=?, antivirus=?, otros=? where idEquipo=?";

        try {
            ps = con.prepareStatement(sql);
            for (int i = 1; i < 12; i++) {
                ps.setString(i, datos[i]);
            }
            ps.setInt(12, Integer.parseInt(datos[0]));
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean eliminar(Equipo equipo) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE equipo SET fhasta=NOW() where idEquipo=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, equipo.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean buscar(Equipo equipo, javax.swing.JTable tabListado) {
        modeloTab = (DefaultTableModel) tabListado.getModel();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM equipo WHERE codActivo like IFNULL(nullif(?,''),'%') and tipo like IFNULL(nullif(?,''),'%') and marca like IFNULL(nullif(?,''),'%') and modelo like IFNULL(nullif(?,''),'%') and serial like IFNULL(nullif(?,''),'%') and procesador like IFNULL(nullif(?,''),'%') and ram like IFNULL(nullif(?,''),'%') and discoduro like IFNULL(nullif(?,''),'%') and nombre like IFNULL(nullif(?,''),'%') and mac like IFNULL(nullif(?,''),'%') and ip like IFNULL(nullif(?,''),'%') and estado like IFNULL(nullif(?,''),'%') and observacion like IFNULL(nullif(?,''),'%')";
        modeloTab.setRowCount(0);

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, equipo.getCodActivo());
            ps.setString(2, equipo.getTipo());
            ps.setString(3, equipo.getMarca());
            ps.setString(4, equipo.getModelo());
            ps.setString(5, equipo.getSerial());
            ps.setString(6, equipo.getProcesador());
            ps.setInt(7, equipo.getRam());
            ps.setInt(8, equipo.getDiscoduro());
            ps.setString(9, equipo.getNombre());
            ps.setString(10, equipo.getMac());
            ps.setString(11, equipo.getIp());
            ps.setString(12, equipo.getEstado());
            ps.setString(13, equipo.getObservacion());

            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);
                for (int j = 0; j < 14; j++) {
                    tabListado.setValueAt(rs.getString(j + 1), i, j);
                }
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public int buscarSotwareAsignado(int idEquipo) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT idEquipo FROM tequipos_software WHERE idEquipo=?";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, idEquipo);
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                pos = rs.getInt("idEquipo");
            }
            return pos;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return pos;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public int buscarIdEquipo(Equipo equipo) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT idEquipo FROM equipo WHERE codActivo=? and fhasta='2999-12-31 00:00:00'";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, equipo.getCodActivo());
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                pos = rs.getInt("idEquipo");
            }
            return pos;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return pos;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean listar(javax.swing.JTable tabListadoSoftware, DefaultTableModel modeloTabSoftware) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM equipo";
        modeloTabSoftware.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTabSoftware.addRow(new Object[i]);
                for (int j = 0; j < 14; j++) {
                    tabListadoSoftware.setValueAt(rs.getString(j + 1), i, j);
                }
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean listarEquiposSoftware(javax.swing.JTable tabListadoSoftware, DefaultTableModel modeloTabSoftware) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "select equipo.idEquipo, equipo.codActivo, equipo.nombre, tequipos_software.usuario, tequipos_software.sisOperativo, tequipos_software.sisOperativo_lic, tequipos_software.office, tequipos_software.office_lic, tequipos_software.visio, tequipos_software.visio_lic, tequipos_software.project, tequipos_software.project_lic, tequipos_software.antivirus, tequipos_software.otros from tequipos_software right join equipo on equipo.idEquipo=tequipos_software.idEquipo";
        modeloTabSoftware.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            ResultSetMetaData rsMd = rs.getMetaData();
            int cantidadColumnas = rsMd.getColumnCount();

            int i = 0;
            while (rs.next()) {
                modeloTabSoftware.addRow(new Object[i]);
                ////
                for (int j = 0; j < 14; j++) {
                    tabListadoSoftware.setValueAt(rs.getString(j + 1), i, j);
                }
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean registrarSoftware(String[] datos) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "INSERT INTO tequipos_software (idEquipo, usuario, sisOperativo, sisOperativo_lic, office, office_lic, visio, visio_lic, project, project_lic, antivirus, otros) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(datos[0]));
            for (int i = 1; i < 12; i++) {
                ps.setString(i + 1, datos[i]);
            }
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

}
