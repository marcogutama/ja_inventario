/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Marco Gutama
 */
public class ConsultasSoftware extends Conexion {

    DefaultTableModel modeloTab;

    public boolean registrar(Software software) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "INSERT INTO SOFTWARE (tipo, nombre, version, cusuario) VALUES (?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, software.getTipo());
            ps.setString(2, software.getNombre());
            ps.setString(3, software.getVersion());
            ps.setString(4, ventanaPrincipal.cusuario);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean modificar(Software software) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE software SET fhasta=NOW() where id=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, software.getId());
            ps.execute();

            sql = "INSERT INTO SOFTWARE (id, tipo, nombre, version, cusuario) VALUES (?,?,?,?,?)";

            ps = con.prepareStatement(sql);
            ps.setInt(1, software.getId());
            ps.setString(2, software.getTipo());
            ps.setString(3, software.getNombre());
            ps.setString(4, software.getVersion());
            ps.setString(5, ventanaPrincipal.cusuario);
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean caducar(Software software) {
        PreparedStatement ps = null;
        Connection con = getConexion();

        String sql = "UPDATE software SET fhasta=NOW(), cusuario=? where id=? and fhasta='2999-12-31 00:00:00'";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, ventanaPrincipal.cusuario);
            ps.setInt(2, software.getId());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean buscar(Software software, javax.swing.JTable tabListado) {
        modeloTab = (DefaultTableModel) tabListado.getModel();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM (SELECT id, nombre, IFNULL(version,'') version, IFNULL(tipo,'') tipo FROM software where fhasta='2999-12-31 00:00:00') listaSoftware WHERE nombre like IFNULL(nullif(?,''),'%') and version like IFNULL(nullif(?,''),'%') and tipo like IFNULL(nullif(?,''),'%') ORDER BY tipo, nombre, version asc;";
        modeloTab.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, software.getNombre());
            ps.setString(2, software.getVersion());
            ps.setString(3, software.getTipo());
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);
                modeloTab.setValueAt(rs.getInt("id"), i, 0);
                modeloTab.setValueAt(rs.getString("nombre"), i, 1);
                modeloTab.setValueAt(rs.getString("version"), i, 2);
                modeloTab.setValueAt(rs.getString("tipo"), i, 3);
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public int buscarIdSoftware(Software software) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT id FROM software WHERE nombre=? and version=?";
        int pos;
        pos = -1;

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, software.getNombre());
            ps.setString(2, software.getVersion());
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                pos = rs.getInt("id");
            }
            return pos;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return pos;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean buscarHistorialSotware(javax.swing.JTable tabControl, javax.swing.JTable tabListado) {
        tabListado.getRowSorter().setSortKeys(null);
        modeloTab = (DefaultTableModel) tabListado.getModel();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "select * from (select ifnull(equipoUsuario.codActivo,'') codActivo, ifnull(equipoUsuario.nombrePC,'') nombrePC, equipoUsuario.fechaUsuarioSoftware,equipoUsuario.tecnico, ifnull(equipoUsuario.usuarioPC,'') usuarioPC,ifnull(concat(equipoUsuario.nombre,' ',equipoUsuario.apellido),'') nombreUsuario, software.nombre nombreSoftware, ifnull(software.version,'') version, estadoSoftware,ifnull(equipoUsuario.licencia,'') licencia from (select * from(select equipo.codActivo,equipo.nombre nombrePC, tequipos_software.idSoftware,tequipos_software.usuarioPC,tequipos_software.licencia, DATE(tequipos_software.fdesde) fechaUsuarioSoftware, if(tequipos_software.fhasta='2999-12-31 00:00:00','Instalado','Desinstalado') estadoSoftware,tequipos_software.cusuario tecnico from tequipos_software, equipo where tequipos_software.idEquipo=equipo.idEquipo and equipo.fhasta='2999-12-31 00:00:00') equipo left join tusuarios_pc on tusuarios_pc.usuario=equipo.usuarioPC and tusuarios_pc.fhasta ='2999-12-31 00:00:00') equipoUsuario, software where equipoUsuario.idSoftware=software.id and software.fhasta ='2999-12-31 00:00:00') equisoft where codActivo like IFNULL(nullif(?,''),'%') and nombrePC like IFNULL(nullif(?,''),'%') and fechaUsuarioSoftware like IFNULL(nullif(?,''),'%') and tecnico like IFNULL(nullif(?,''),'%') and usuarioPC like IFNULL(nullif(?,''),'%') and estadoSoftware like IFNULL(nullif(?,''),'%') and nombreSoftware like IFNULL(nullif(?,''),'%') and version like IFNULL(nullif(?,''),'%') and licencia like IFNULL(nullif(?,''),'%') ORDER BY codActivo;";
        modeloTab.setRowCount(0);

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, (String) tabControl.getValueAt(0, 0));
            ps.setString(2, (String) tabControl.getValueAt(0, 1));
            ps.setString(3, (String) tabControl.getValueAt(0, 2));
            ps.setString(4, (String) tabControl.getValueAt(0, 3));
            ps.setString(5, (String) tabControl.getValueAt(0, 4));
            ps.setString(6, (String) tabControl.getValueAt(0, 5));
            ps.setString(7, (String) tabControl.getValueAt(0, 6));
            ps.setString(8, (String) tabControl.getValueAt(0, 7));
            ps.setString(9, (String) tabControl.getValueAt(0, 8));

            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                modeloTab.addRow(new Object[i]);
                tabListado.setValueAt(rs.getString("codActivo"), i, 0);
                tabListado.setValueAt(rs.getString("nombrePC"), i, 1);
                tabListado.setValueAt(rs.getString("fechaUsuarioSoftware"), i, 2);
                tabListado.setValueAt(rs.getString("tecnico"), i, 3);
                tabListado.setValueAt(rs.getString("usuarioPC"), i, 4);
                tabListado.setValueAt(rs.getString("estadoSoftware"), i, 5);
                tabListado.setValueAt(rs.getString("nombreSoftware"), i, 6);
                tabListado.setValueAt(rs.getString("version"), i, 7);               
                tabListado.setValueAt(rs.getString("licencia"), i, 8);
                i++;                
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public JComboBox llenarComboBox(String tipo) {
        JComboBox comboBox = new JComboBox();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT nombre, version FROM software where tipo=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, tipo);
            rs = ps.executeQuery();

            while (rs.next()) {
                comboBox.addItem(rs.getString(1) + " " + rs.getString(2));
            }
            return comboBox;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return comboBox;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    public JComboBox llenarComboBoxTipo() {
        JComboBox comboBox = new JComboBox();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT tipo FROM software WHERE fhasta='2999-12-31 00:00:00' GROUP BY tipo";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                comboBox.addItem(rs.getString(1));
            }
            return comboBox;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return comboBox;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public Software[] listaSO() {
        Software[] lista = new Software[20];
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT id, nombre, version FROM software where tipo='S.O.'";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            int i = 0;
            while (rs.next()) {
                lista[i] = new Software();
                lista[i].setId(rs.getInt("id"));
                lista[i].setNombre(rs.getString("nombre"));
                lista[i].setVersion(rs.getString("version"));
                i++;
            }
            return lista;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return lista;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public boolean listar(javax.swing.JTable tabListadoSoftware, DefaultTableModel modeloTabSoftware) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT * FROM software ORDER BY tipo, nombre, version asc";
        modeloTabSoftware.setRowCount(0);
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            ResultSetMetaData rsMd = rs.getMetaData();
            int cantidadColumnas = rsMd.getColumnCount();

            int i = 0;
            while (rs.next()) {
                modeloTabSoftware.addRow(new Object[i]);
                tabListadoSoftware.setValueAt(rs.getInt("id"), i, 0);
                tabListadoSoftware.setValueAt(rs.getString("nombre"), i, 1);
                tabListadoSoftware.setValueAt(rs.getString("version"), i, 2);
                tabListadoSoftware.setValueAt(rs.getString("tipo"), i, 3);
                i++;
            }
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

}
