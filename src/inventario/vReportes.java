/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import com.mysql.jdbc.Connection;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultCellEditor;
import javax.swing.table.TableColumn;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.FileResolver;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Marco Gutama
 */
public class vReportes extends javax.swing.JDialog {

    ConsultasOficinas conOficinas;
    public UsuarioLab usuario;

    /**
     * Creates new form vReportes
     */
    public vReportes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

        conOficinas = new ConsultasOficinas();
        cbOficinaReporte.setModel(conOficinas.llenarCombBox().getModel());
        cbOficinaReporte.insertItemAt(null, 0);
        cbOficinaReporte.setSelectedIndex(0);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jLabel7 = new javax.swing.JLabel();
        rbHardware = new javax.swing.JRadioButton();
        rbComputadoras = new javax.swing.JRadioButton();
        rbPerifericos = new javax.swing.JRadioButton();
        jButton2 = new javax.swing.JButton();
        rbMateriales = new javax.swing.JRadioButton();
        rbGLPI = new javax.swing.JRadioButton();
        rbSoftware = new javax.swing.JRadioButton();
        jLabel10 = new javax.swing.JLabel();
        rbPdf = new javax.swing.JRadioButton();
        rbExcel = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        cbOficinaReporte = new javax.swing.JComboBox<>();
        txtMsg = new javax.swing.JLabel();
        rbMaterialesH = new javax.swing.JRadioButton();
        rbGLPIsoftware = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jdcfFin = new com.toedter.calendar.JDateChooser();
        jdcfInicio = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Reportes");

        jLabel7.setText("Seleccionar el reporte a generar:");

        buttonGroup1.add(rbHardware);
        rbHardware.setSelected(true);
        rbHardware.setText("Hardware");

        buttonGroup1.add(rbComputadoras);
        rbComputadoras.setText("Computadoras");

        buttonGroup1.add(rbPerifericos);
        rbPerifericos.setText("Periféricos");

        jButton2.setText("Generar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbMateriales);
        rbMateriales.setText("Materiales");

        buttonGroup1.add(rbGLPI);
        rbGLPI.setText("GLPIs");

        buttonGroup1.add(rbSoftware);
        rbSoftware.setText("Software");

        jLabel10.setText("Formato:");

        buttonGroup2.add(rbPdf);
        rbPdf.setSelected(true);
        rbPdf.setText("PDF");

        buttonGroup2.add(rbExcel);
        rbExcel.setText("Excel");

        jLabel2.setText("Oficina:");

        txtMsg.setText(" ");

        buttonGroup1.add(rbMaterialesH);
        rbMaterialesH.setText("Materiales Movimientos");

        buttonGroup1.add(rbGLPIsoftware);
        rbGLPIsoftware.setText("GLPIs software");

        jLabel1.setText("Filtros:");

        jLabel3.setText("Desde:");

        jLabel8.setText("Hasta:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(txtMsg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbHardware)
                            .addComponent(rbComputadoras)
                            .addComponent(rbPerifericos)
                            .addComponent(rbSoftware))
                        .addGap(16, 16, 16)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbGLPI)
                            .addComponent(rbGLPIsoftware)
                            .addComponent(rbMateriales)
                            .addComponent(rbMaterialesH)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel7))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(189, 189, 189)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(13, 13, 13)
                        .addComponent(rbPdf)
                        .addGap(18, 18, 18)
                        .addComponent(rbExcel))
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbOficinaReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jdcfInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jdcfFin, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(27, 27, 27))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel10)
                    .addComponent(rbPdf)
                    .addComponent(rbExcel)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(rbHardware, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
                    .addComponent(rbGLPI, javax.swing.GroupLayout.DEFAULT_SIZE, 24, Short.MAX_VALUE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbGLPIsoftware, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(cbOficinaReporte, javax.swing.GroupLayout.Alignment.CENTER, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rbComputadoras, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(rbPerifericos, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                    .addComponent(rbMateriales, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                    .addComponent(jLabel3)
                    .addComponent(jdcfInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(rbSoftware, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                    .addComponent(rbMaterialesH, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                    .addComponent(jLabel8)
                    .addComponent(jdcfFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtMsg))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        String tipoReporte = null;
        java.sql.Timestamp fDesde = null;
        java.sql.Timestamp fHasta = null;

        Date dateInicio = jdcfInicio.getDate();
        Date dateFin = jdcfFin.getDate();
        try {
            if (dateInicio == null) {
                dateInicio = new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01");
            }
            if (dateFin == null) {
                dateFin = new SimpleDateFormat("yyyy-MM-dd").parse("2999-01-01");
            }
            dateInicio.setHours(0);
            dateInicio.setMinutes(0);
            dateInicio.setSeconds(0);
        } catch (ParseException ex) {
            Logger.getLogger(vFechas.class.getName()).log(Level.SEVERE, null, ex);
        }
        fDesde = new java.sql.Timestamp(dateInicio.getTime());
        fHasta = new java.sql.Timestamp(dateFin.getTime());

        if (rbHardware.isSelected()) {
            tipoReporte = "Hardware";
        } else if (rbPerifericos.isSelected()) {
            tipoReporte = "Perifericos";
        } else if (rbComputadoras.isSelected()) {
            tipoReporte = "Computadoras";
        } else if (rbMateriales.isSelected()) {
            tipoReporte = "Materiales";
        } else if (rbMaterialesH.isSelected()) {
            tipoReporte = "Materiales_hist";
        } else if (rbSoftware.isSelected()) {
            tipoReporte = "Software";
        } else if (rbGLPI.isSelected()) {
            tipoReporte = "GLPI";
        } else if (rbGLPIsoftware.isSelected()) {
            tipoReporte = "GLPI_Software";
        }

        try {
            Conexion con = new Conexion();
            Connection conn = (Connection) con.getConexion();

            JasperReport reporte = null;
            //tring path = "src\\reportes\\" + tipoReporte + ".jasper";
            String path = "/reportes/" + tipoReporte + ".jasper";
            InputStream is = null;
            is = getClass().getResourceAsStream(path);

            Map parametro = new HashMap();
            parametro.put("FDESDE", fDesde);
            parametro.put("FHASTA", fHasta);
            parametro.put("USR", ventanaPrincipal.cusuario);
            parametro.put("idZona", ventanaPrincipal.czona);
            parametro.put("ZONA", usuario.getNombre_zona());

            if (cbOficinaReporte.getSelectedIndex() > 0) {
                Oficina ofiId = (Oficina) cbOficinaReporte.getSelectedItem();
                parametro.put("idOficina", ofiId.getId());
            } else {
                parametro.put("idOficina", null);
            }

            //reporte = (JasperReport) JRLoader.loadObjectFromFile(path);
            JasperPrint jprint = JasperFillManager.fillReport(is, parametro, conn);
            //            JasperViewer view = new JasperViewer(jprint, false);
            //            view.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            //            view.setVisible(true);
            ///////

            String nombreReporte;
            Date date = new Date();
            DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss ");
            nombreReporte = fechaHora.format(date);
            nombreReporte = nombreReporte + tipoReporte;

            if (rbPdf.isSelected()) {
                JasperExportManager.exportReportToPdfFile(jprint, nombreReporte + ".pdf");
                txtMsg.setText("Reporte " + nombreReporte + ".pdf" + " creado.");
            } else {
                JRXlsxExporter exporter = new JRXlsxExporter();
                exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jprint);
                exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, nombreReporte + ".xlsx");

                exporter.exportReport();
                txtMsg.setText("Reporte " + nombreReporte + ".xlsx" + " creado.");

            }

            /////
        } catch (Exception ex) {
            Logger.getLogger(ventanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(vReportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(vReportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(vReportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(vReportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                vReportes dialog = new vReportes(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JComboBox<Oficina> cbOficinaReporte;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private com.toedter.calendar.JDateChooser jdcfFin;
    private com.toedter.calendar.JDateChooser jdcfInicio;
    private javax.swing.JRadioButton rbComputadoras;
    private javax.swing.JRadioButton rbExcel;
    private javax.swing.JRadioButton rbGLPI;
    private javax.swing.JRadioButton rbGLPIsoftware;
    private javax.swing.JRadioButton rbHardware;
    private javax.swing.JRadioButton rbMateriales;
    private javax.swing.JRadioButton rbMaterialesH;
    private javax.swing.JRadioButton rbPdf;
    private javax.swing.JRadioButton rbPerifericos;
    private javax.swing.JRadioButton rbSoftware;
    private javax.swing.JLabel txtMsg;
    // End of variables declaration//GEN-END:variables
}
