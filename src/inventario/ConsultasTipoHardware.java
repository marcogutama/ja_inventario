/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author usuario
 */
public class ConsultasTipoHardware extends Conexion {

    public JComboBox llenarComboBox() {
        JComboBox<String> comboBox = new JComboBox();
        comboBox.setEditable(true);
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT tipo FROM equipo WHERE fhasta='2999-12-31 00:00:00' GROUP BY tipo;";
        try {

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                comboBox.addItem(rs.getString("tipo"));
            }
            return comboBox;

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return comboBox;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    public JComboBox llenarComboBoxMarca() {
        JComboBox<String> comboBox = new JComboBox();
        comboBox.setEditable(true);
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT marca FROM equipo WHERE fhasta='2999-12-31 00:00:00' GROUP BY marca;";
        try {

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                comboBox.addItem(rs.getString("marca"));
            }
            return comboBox;

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            return comboBox;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

}
