-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-09-2022 a las 04:58:54
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_inventario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tzonas`
--

CREATE TABLE `tzonas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(31) NOT NULL,
  `fhasta` datetime NOT NULL DEFAULT '2999-12-31 00:00:00',
  `fdesde` datetime NOT NULL DEFAULT current_timestamp(),
  `cusuario` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tzonas`
--

INSERT INTO `tzonas` (`id`, `nombre`, `fhasta`, `fdesde`, `cusuario`) VALUES
(1, 'CUENCA', '2999-12-31 00:00:00', '2021-03-22 22:23:03', 'm.gutama'),
(2, 'CAÑAR', '2999-12-31 00:00:00', '2021-03-22 22:23:03', 'm.gutama'),
(3, 'ORIENTE', '2999-12-31 00:00:00', '2021-03-22 22:23:03', 'm.gutama'),
(4, 'JUBONES', '2999-12-31 00:00:00', '2021-03-22 22:23:03', 'm.gutama'),
(5, 'NORTE', '2999-12-31 00:00:00', '2021-03-22 22:23:03', 'm.gutama'),
(6, 'COSTA', '2999-12-31 00:00:00', '2021-03-22 22:23:03', 'm.gutama');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tzonas`
--
ALTER TABLE `tzonas`
  ADD PRIMARY KEY (`id`,`fhasta`),
  ADD KEY `FK_CUSUARIO_ZONA` (`cusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tzonas`
--
ALTER TABLE `tzonas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tzonas`
--
ALTER TABLE `tzonas`
  ADD CONSTRAINT `FK_CUSUARIO_ZONA` FOREIGN KEY (`cusuario`) REFERENCES `tusuarios_lab` (`usuario`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
ALTER TABLE `equipo` ADD `idZona` int(11) NOT NULL AFTER `nombre`;
--
-- Indices de la tabla `tusuarios_lab`
--
ALTER TABLE `tusuarios_lab`
  ADD KEY `FK_USUARIOLAB_ZONA` (`idZona`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tusuarios_lab`
--
ALTER TABLE `tusuarios_lab`
  ADD CONSTRAINT `FK_USUARIOLAB_ZONA` FOREIGN KEY (`idZona`) REFERENCES `tzonas` (`id`) ON UPDATE CASCADE;
COMMIT; 

