INSERT INTO equipo ( codActivo,  tipo,  marca, modelo,  serial,  procesador,ram,  discoduro,  nombre,  mac,  ip,  estado,  observacion) VALUES
("1.8.06.01.009.3234.001", "CPU", "ASUS", "", "", "", "", "", "", "", "", "", ""),
("1.8.06.01.009.3234.001", "CPU", "GIGA", "", "", "", "", "", "", "", "", "", ""),
("1.8.06.01.009.0005.015", "PORTATIL", "TOSHIBA", "", "", "", "", "", "", "", "", "", ""),
("1.8.06.01.009.2238.099", "PORTATIL", "LENOVO", "", "", "", "", "", "", "", "", "", ""),
("1.8.06.01.009.2238.001", "MONITOR", "LG", "", "", "", "", "", "", "", "", "", ""),
("1.8.06.01.009.1409.001", "MONITOR", "LG", "", "", "", "", "", "", "", "", "", ""),
("1.8.06.01.009.1409.001", "SCANER", "GUTAMA", "", "", "", "", "", "", "", "", "", ""),
("1.8.06.01.009.1303.015", "SCANER", "SAMSUNG", "", "", "", "", "", "", "", "", "", ""),
("1.8.06.01.009.1303.013", "IMPRESORA", "EPSON", "FX890", "", "", "", "", "", "", "", "", ""),
("1.8.06.01.009.1303.021", "IMPRESORA", "EPSON", "FX890II", "", "", "", "", "", "", "", "", "");

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `jsp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empledos`
--

CREATE TABLE `oficina` (
  `idOficina` varchar(3) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `zona` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empledos`
--

INSERT INTO `oficina` (`idOficina`, `nombre`, `zona`) VALUES
("CUE", "HURTADO DE MOENDOZA", "CUENCA"),
("CUE", "BENIGNO MALO", "CUENCA"),
("CUE", "TOTORACOCHA", "CUENCA"),
("CUE", "VERGEL", "CUENCA"),
("GUA", "GUALACEO", "CUENCA");



-----------------------------
CREATE TABLE `Toficina_Equipos` (
  `idOficina` varchar(3) NOT NULL,
  `departamento` varchar(3) NOT NULL,
  `codActivo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `Toficina_Equipos` (`idOficina`, `departamento`, `codActivo`) VALUES
("GUA", "Cajas", "1.8.06.01.009.1303.021");

select * from Toficina_Equipos right join equipo on Toficina_Equipos.codActivo=equipo.codActivo
select oficina.nombre, oficina.zona, Toficina_Equipos.departamento from Toficina_Equipos, oficina where Toficina_Equipos.idOficina=oficina.idOficina

select oficina.nombre, oficina.zona, Toficina_Equipos.departamento from oficina,Toficina_Equipos right join equipo on  Toficina_Equipos.idOficina=oficina.idOficina and equipo.codActivo=Toficina_Equipos.codActivo

select equiposoft.idEquipo, equiposoft.codActivo, equiposoft.nombre, software.tipo, software.nombre,software.version from (select equipo.idEquipo, equipo.codActivo, equipo.nombre, tequipos_software.idSoftware from equipo, tequipos_software where equipo.idEquipo=tequipos_software.idEquipo) equiposoft right JOIN software ON equiposoft.idEquipo = tequipos_software.idEquipo

CREATE TABLE software (
    id int NOT NULL AUTO_INCREMENT,
    tipo varchar(30) NOT NULL,
    nombre varchar(30),
    version varchar(20),
    PRIMARY KEY (id)
);

CREATE TABLE tequipos_software (
    idequipo int NOT NULL AUTO_INCREMENT,
    idsoftware int NOT NULL,
    usuario varchar(30)
);

INSERT INTO `tequipos_software` (`idequipo`, `idsoftware`, `usuario`) VALUES
(1, 1, "m.gutama"),
(2, 2, "p.morocho");
INSERT INTO `tequipos_software` (`idequipo`, `idsoftware`, `usuario`) VALUES
(2, 9, "p.morocho");

ALTER TABLE equipo 
ADD idequipo int NOT NULL;

ALTER TABLE equipo
   CHANGE idequipo INT NOT NULL FIRST;
   
   ALTER TABLE equipo MODIFY COLUMN idequipo int first
   
   DROP TABLE equipo;
   
   CREATE TABLE equipo (
    idEquipo int NOT NULL AUTO_INCREMENT,
    codActivo varchar(30) NOT NULL,
    tipo varchar(30),
    marca varchar(20),
	modelo varchar(30),
    serial varchar(30),
    procesador varchar(20),
	ram varchar(30),
    discoduro varchar(30),
    nombre varchar(20),
	mac varchar(30),
    ip varchar(30),
    estado varchar(20),
	observacion varchar(30),
    fdesde Date,
    fhasta Date,
    PRIMARY KEY (idEquipo)
);

select equipo.idEquipo, equipo.codActivo, equipo.nombre, tequipos_software.idSoftware from equipo, tequipos_software where equipo.idEquipo=tequipos_software.idEquipo

select equiposoft.idEquipo, equiposoft.codActivo, equiposoft.nombre from 
(select equipo.idEquipo, equipo.codActivo, equipo.nombre, tequipos_software.idSoftware from equipo, tequipos_software 
where equipo.idEquipo=tequipos_software.idEquipo) equiposoft 
right JOIN equipo ON equiposoft.idEquipo = equipo.idEquipo

select tequipos_software.idEquipo, software.id, software.tipo, software.nombre, software.version from tequipos_software,software where tequipos_software.idSoftware=software.id

select equipo.idEquipo,equipo.codActivo,equipo.nombre,soft.tipo, soft.nombre, soft.version from (select tequipos_software.idEquipo, software.id, software.tipo, software.nombre, software.version 
from tequipos_software,software where tequipos_software.idSoftware=software.id) soft right join equipo on equipo.idEquipo=soft.idEquipo

INSERT INTO equipo ( codActivo,  tipo,  marca, modelo,  serial,  procesador,ram,  discoduro,  nombre,  mac,  ip,  estado,  observacion, fdesde,fhasta) VALUES
("1.8.06.01.009.3234.001", "CPU", "GIGA", "", "", "", "", "", "", "", "", "", "",NOW(),null),
("1.8.06.01.009.0005.015", "PORTATIL", "TOSHIBA", "", "", "", "", "", "", "", "", "", "",NOW(),null),
("1.8.06.01.009.2238.099", "PORTATIL", "LENOVO", "", "", "", "", "", "", "", "", "", "",NOW(),null),
("1.8.06.01.009.2238.001", "MONITOR", "LG", "", "", "", "", "", "", "", "", "", "",NOW(),null),
("1.8.06.01.009.1409.001", "MONITOR", "LG", "", "", "", "", "", "", "", "", "", "",NOW(),null),
("1.8.06.01.009.1409.001", "SCANER", "GUTAMA", "", "", "", "", "", "", "", "", "", "",NOW(),null),
("1.8.06.01.009.1303.015", "SCANER", "SAMSUNG", "", "", "", "", "", "", "", "", "", "",NOW(),null),
("1.8.06.01.009.1303.013", "IMPRESORA", "EPSON", "FX890", "", "", "", "", "", "", "", "", "",NOW(),null),
("1.8.06.01.009.1303.021", "IMPRESORA", "EPSON", "FX890II", "", "", "", "", "", "", "", "", "",NOW(),null);

DROP TABLE tequipos_software;

CREATE TABLE tequipos_software (
    idEquipo int NOT NULL,
	usuario varchar(30),
    sisOperativo varchar(30) NOT NULL,
    sisOperativo_lic varchar(30),
    office varchar(30),
    office_lic varchar(30),
    visio varchar(20),
    visio_lic varchar(30),
	project varchar(30),
    project_lic varchar(30),
    antivirus varchar(30),
    otros varchar(20),
    PRIMARY KEY (idEquipo)
);

select equipo.idEquipo, equipo.codActivo, equipo.nombre, tequipos_software.usuario from tequipos_software right join equipo on equipo.idEquipo=tequipos_software.idEquipo

DROP TABLE oficina;

CREATE TABLE oficina (
    id int NOT NULL AUTO_INCREMENT,    
    nombre varchar(30),
	abreviatura varchar(20),
    zona varchar(20),
    PRIMARY KEY (id)
);

INSERT INTO `oficina` (`abreviatura`, `nombre`, `zona`) VALUES
("HTM", "HURTADO DE MOENDOZA", "CUENCA"),
("BNM", "BENIGNO MALO", "CUENCA"),
("TOT", "TOTORACOCHA", "CUENCA"),
("VER", "VERGEL", "CUENCA"),
("GUA", "GUALACEO", "CUENCA");

SELECT * FROM oficina WHERE nombre like ifnull(null,'%');

SELECT * FROM oficina WHERE nombre like ifnull(nullif(null,''),'%')

DROP TABLE toficina_equipos;

CREATE TABLE toficina_equipos (
    idOficina int NOT NULL,
	idEquipo int,
    departamento varchar(30)
);

INSERT INTO toficina_equipos (idOficina, idEquipo, departamento) VALUES
(2, 3, "CAJAS"),
(3, 4, "SERVICIOS");

select equiposoficina.idEquipo,equiposoficina.codActivo, equiposoficina.tipo, equiposoficina.marca, equiposoficina.nombre, equiposoficina.departamento from (SELECT equipo.idEquipo,equipo.codActivo, equipo.tipo,equipo.marca, ofi.nombre, ofi.departamento from ( SELECT oficina.nombre, oficina.zona, Toficina_Equipos.departamento, Toficina_Equipos.idEquipo FROM Toficina_Equipos, oficina WHERE Toficina_Equipos.idOficina = oficina.id ) ofi right JOIN equipo ON ofi.idEquipo = equipo.idEquipo) equiposoficina where equiposoficina.codActivo like IFNULL(nullif(null,''),'%') and equiposoficina.tipo like IFNULL(nullif(null,''),'%') and equiposoficina.marca like IFNULL(nullif(null,''),'%') and equiposoficina.nombre like nullif(null,'')


select * from (select equipo.codActivo,equipo.nombre, tequipos_software.usuario,tequipos_software.sisOperativo,tequipos_software.sisOperativo_lic,tequipos_software.office,tequipos_software.office_lic,tequipos_software.visio,tequipos_software.visio_lic,tequipos_software.project,tequipos_software.project_lic,tequipos_software.antivirus,tequipos_software.otros  from tequipos_software, equipo where tequipos_software.idEquipo=equipo.idEquipo) equisoft where equisoft.codActivo like IFNULL(nullif(?,''),'%') and equisoft.nombre like IFNULL(nullif(?,''),'%') and equisoft.usuario like IFNULL(nullif(?,''),'%') and equisoft.sisOperativo like IFNULL(nullif(?,''),'%') and equisoft.sisOperativo_lic like IFNULL(nullif(?,''),'%') and equisoft.office like IFNULL(nullif(?,''),'%') and equisoft.office_lic like IFNULL(nullif(?,''),'%') and equiposoft.visio like IFNULL(nullif(?,''),'%') and equisoft.visio_lic like IFNULL(nullif(?,''),'%') and equisoft.project like IFNULL(nullif(?,''),'%') and equisoft.project_lic like IFNULL(nullif(?,''),'%') and equisoft.antivirus like IFNULL(nullif(?,''),'%') and equisoft.otros like IFNULL(nullif(?,''),'%');


select equipo.idEquipo, equipo.codActivo, equipo.nombre, tequipos_software.* from tequipos_software right join equipo on equipo.idEquipo=tequipos_software.idEquipo

select * from (select equipo.idEquipo, equipo.codActivo, equipo.nombre, tequipos_software.* from tequipos_software right join equipo on equipo.idEquipo=tequipos_software.idEquipo) equisoft where equisoft.codActivo like IFNULL(nullif(?,''),'%') and equisoft.nombre like IFNULL(nullif(?,''),'%') and equisoft.usuario like IFNULL(nullif(?,''),'%') or nullif(?,'') is null and equisoft.sisOperativo like IFNULL(nullif(?,''),'%') or nullif(?,'') is null and equisoft.sisOperativo_lic like IFNULL(nullif(?,''),'%') or nullif(?,'') is null and equisoft.office like IFNULL(nullif(?,''),'%') or nullif(?,'') is null and equisoft.office_lic like IFNULL(nullif(?,''),'%') or nullif(?,'') is null and equisoft.visio like IFNULL(nullif(?,''),'%') or nullif(?,'') is null and equisoft.visio_lic like IFNULL(nullif(?,''),'%') or nullif(?,'') is null and equisoft.project like IFNULL(nullif(?,''),'%') or nullif(?,'') is null and equisoft.project_lic like IFNULL(nullif(?,''),'%') or nullif(?,'') is null and equisoft.antivirus like IFNULL(nullif(?,''),'%') or nullif(?,'') is null and equisoft.otros like IFNULL(nullif(?,''),'%') or nullif(?,'') is null;


select * from (select equipo.idEquipo, equipo.codActivo, equipo.nombre, tequipos_software.usuario,tequipos_software.sisOperativo,tequipos_software.sisOperativo_lic,tequipos_software.office,tequipos_software.office_lic,tequipos_software.visio,tequipos_software.visio_lic,tequipos_software.project,tequipos_software.project_lic,tequipos_software.antivirus,tequipos_software.otros from tequipos_software right join equipo on equipo.idEquipo=tequipos_software.idEquipo) equisoft where equisoft.codActivo like IFNULL(nullif(null,''),'%') and equisoft.nombre like IFNULL(nullif(null,''),'%') and equisoft.usuario like IFNULL(nullif(null,''),'%') or nullif(null,'') is null and equisoft.sisOperativo like IFNULL(nullif(null,''),'%') or nullif(null,'') is null and equisoft.sisOperativo_lic like IFNULL(nullif(null,''),'%') or nullif(null,'') is null and equisoft.office like IFNULL(nullif(null,''),'%') or nullif(null,'') is null and equisoft.office_lic like IFNULL(nullif(null,''),'%') or nullif(null,'') is null and equisoft.visio like IFNULL(nullif(null,''),'%') or nullif(null,'') is null and equisoft.visio_lic like IFNULL(nullif(null,''),'%') or nullif(null,'') is null and equisoft.project like IFNULL(nullif(null,''),'%') or nullif(null,'') is null and equisoft.project_lic like IFNULL(nullif(null,''),'%') or nullif(null,'') is null and equisoft.antivirus like IFNULL(nullif(null,''),'%') or nullif(null,'') is null and equisoft.otros like IFNULL(nullif(null,''),'%') or nullif(null,'') is null;


CREATE TABLE glpi (
    id int NOT NULL AUTO_INCREMENT, 
idEquipo int,
fecha Date,	
    num_glpi varchar(30),
	tecnico varchar(20),
    descripcion varchar(20),
    PRIMARY KEY (id)
);

INSERT INTO toficina_equipos (idOficina, idEquipo, departamento) VALUES
(2, 3, "CAJAS"),
(3, 4, "SERVICIOS");

select * from equipo, glpi where equipo.idEquipo=glpi.idEquipo

select equipo.codActivo, equipo.tipo, glpi.* from equipo, glpi where equipo.idEquipo=glpi.idEquipo

select * from (select equipo.codActivo, equipo.tipo, glpi.* from equipo, glpi where equipo.idEquipo=glpi.idEquipo) listGlpi where listGlpi.tecnico like '%Adri%'

select listGlpi.id, listGlpi.fecha, listGlpi.codActivo, listGlpi.tipo, listGlpi.tecnico, listGlpi.num_glpi, listGlpi.descripcion from (select equipo.codActivo, equipo.tipo, glpi.* from equipo, glpi where equipo.idEquipo=glpi.idEquipo) listGlpi where listGlpi.fecha like IFNULL(nullif(?,''),'%') and listGlpi.codActivo like IFNULL(nullif(?,''),'%') and listGlpi.tipo like IFNULL(nullif(?,''),'%') and listGlpi.tecnico like IFNULL(nullif(?,''),'%') and listGlpi.num_glpi like IFNULL(nullif(?,''),'%') and and listGlpi.descripcion like IFNULL(nullif(?,''),'%');

select * from (SELECT equipo.*, ofi.idOficina, ofi.nombreOfi, ofi.departamento from ( SELECT oficina.id idOficina, oficina.nombre nombreOfi, Toficina_Equipos.departamento, Toficina_Equipos.idEquipo FROM Toficina_Equipos, oficina WHERE Toficina_Equipos.idOficina = oficina.id ) ofi right JOIN equipo ON ofi.idEquipo = equipo.idEquipo) equiposoficina where codActivo like IFNULL(nullif(?,''),'%') and tipo like IFNULL(nullif(?,''),'%') and marca like IFNULL(nullif(?,''),'%') and modelo like IFNULL(nullif(?,''),'%') and serial like IFNULL(nullif(?,''),'%') and procesador like IFNULL(nullif(?,''),'%') and ram like IFNULL(nullif(?,''),'%') and discoduro like IFNULL(nullif(?,''),'%') and nombre like IFNULL(nullif(?,''),'%') and mac like IFNULL(nullif(?,''),'%') and ip like IFNULL(nullif(?,''),'%') and estado like IFNULL(nullif(?,''),'%') and observacion like IFNULL(nullif(?,''),'%') and equiposoficina.nombreOfi like nullif(?,'') or nullif(?,'') is null

select * from (SELECT equipo.*, IfNull(ofi.idOficina,'') idOficina, IfNull(ofi.nombreOfi,'') nombreOfi, IfNull(ofi.departamento,'') departamento from ( SELECT oficina.id idOficina, oficina.nombre nombreOfi, Toficina_Equipos.departamento, Toficina_Equipos.idEquipo FROM Toficina_Equipos, oficina WHERE Toficina_Equipos.idOficina = oficina.id ) ofi right JOIN equipo ON ofi.idEquipo = equipo.idEquipo) equiposoficina where codActivo like IFNULL(nullif(?,''),'%') and tipo like IFNULL(nullif(?,''),'%') and marca like IFNULL(nullif(?,''),'%') and modelo like IFNULL(nullif(?,''),'%') and serial like IFNULL(nullif(?,''),'%') and procesador like IFNULL(nullif(?,''),'%') and ram like IFNULL(nullif(?,''),'%') and discoduro like IFNULL(nullif(?,''),'%') and nombre like IFNULL(nullif(?,''),'%') and mac like IFNULL(nullif(?,''),'%') and ip like IFNULL(nullif(?,''),'%') and estado like IFNULL(nullif(?,''),'%') and observacion like IFNULL(nullif(?,''),'%') and equiposoficina.nombreOfi like IFNULL(nullif(?,''),'%')

CREATE TABLE materiales (
    id int NOT NULL AUTO_INCREMENT, 
codigo varchar(20),
descripcion varchar(20),
    PRIMARY KEY (id)
);
DROP TABLE tmovimientos;
CREATE TABLE tmovimientos (
    id int NOT NULL AUTO_INCREMENT,
idMaterial int,	
fecha Date,
tipomovimiento varchar(30),
cantidad int,    
	antStock int,
	nuevoStock int,
	observacion varchar(100),
    PRIMARY KEY (id)
);


INSERT INTO tmovimientos (idMaterial, fecha,tipomovimiento,cantidad,stock,observacion) VALUES
(7, NOW(), "Entrada",10,6,"Prueba"),
(7, NOW(), "Salida",2,16,"Prueba");

select * from (select idMaterial,tipomovimiento, sum(cantidad) from tmovimientos WHERE tipomovimiento="Entrada" group by idMaterial) totEntradas left join (select idMaterial,tipomovimiento, sum(cantidad) from tmovimientos WHERE tipomovimiento="Salida" group by idMaterial) totSalidas on totEntradas.idMaterial=totSalidas.idMaterial

select totEntradas.idMaterial,totEntradas.tipomovimiento, totEntradas.totalEntradas,totSalidas.tipomovimiento, totSalidas.totalSalidas from (select idMaterial,tipomovimiento, sum(cantidad) total from tmovimientos WHERE tipomovimiento="Entrada" group by idMaterial) totEntradas left join (select idMaterial,tipomovimiento, sum(cantidad) totalSalidas from tmovimientos WHERE tipomovimiento="Salida" group by idMaterial) totSalidas on totEntradas.idMaterial=totSalidas.idMaterial

select totEntradas.idMaterial,totEntradas.tipomovimiento, totEntradas.totalEntradas,totSalidas.tipomovimiento, totSalidas.totalSalidas from (select idMaterial,tipomovimiento, sum(cantidad) totalEntradas from tmovimientos WHERE tipomovimiento="Entrada" group by idMaterial) totEntradas left join (select idMaterial,tipomovimiento, sum(cantidad) totalSalidas from tmovimientos WHERE tipomovimiento="Salida" group by idMaterial) totSalidas on totEntradas.idMaterial=totSalidas.idMaterial

select * from materiales join (select totEntradas.idMaterial, totEntradas.totalEntradas, totSalidas.totalSalidas from (select idMaterial,tipomovimiento, sum(cantidad) totalEntradas from tmovimientos WHERE tipomovimiento="Entrada" group by idMaterial) totEntradas left join (select idMaterial,tipomovimiento, sum(cantidad) totalSalidas from tmovimientos WHERE tipomovimiento="Salida" group by idMaterial) totSalidas on totEntradas.idMaterial=totSalidas.idMaterial) totalMovimientos on totalMovimientos.idMaterial=materiales.id

DROP TABLE tusuarios;

CREATE TABLE tusuarios (
    id int NOT NULL AUTO_INCREMENT,
	usuarioId varchar(30),	
	nombre varchar(30),
	apellido varchar(30),
    PRIMARY KEY (id)
);

INSERT INTO tusuarios (nombre,apellido,usuario) VALUES
("Pedro", "Suarez", "p.suarez"),
("Julio", "Perez", "j.perez"),
("Rosa", "Linda", "r.linda"),
("Yasuri", "Hidalgo", "y.hidalgo"),
("Jose", "Martinez", "j.martinez"),
("Isabel", "Quichimbo", "i.quichimbo"),
("Camila", "Ochoa", "c.ochoa"),
("Martina", "Reino", "m.reino");

DROP TABLE tequipos_software;

CREATE TABLE tequipos_software (
    idEquipo int,
	idUsuario int,	
	idSoftware int,
	licencia varchar(30),
);

ALTER TABLE glpi ADD CONSTRAINT fk_GlpiEquipo FOREIGN KEY (idEquipo) REFERENCES equipo (idEquipo) ON DELETE CASCADE ON UPDATE CASCADE
ALTER TABLE `glpi` ADD  CONSTRAINT `fk_GlpiEquipo` FOREIGN KEY (`idEquipo`) REFERENCES `equipo`(`idEquipo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE glpi FOREIGN KEY `fk_GlpiEquipo`

ALTER TABLE `tmovimientos` ADD  CONSTRAINT `fk_MovimientoMaterial` FOREIGN KEY (`id`) REFERENCES `materiales`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

select tusuarios.id idUsuario,tusuarios.usuario,tusuarios.nombre,tusuarios.apellido, software.*,tequipos_software.licencia from tequipos_software, tusuarios, software where tequipos_software.idEquipo=31 and tequipos_software.idUsuario=tusuarios.id and tequipos_software.idSoftware=software.id

select * from (SELECT equiposPC.*, IfNull(ofi.idOficina,'') idOficina, IfNull(ofi.nombreOfi,'') nombreOfi, IfNull(ofi.departamento,'') departamento,IfNull(ofi.zona,'') zona from ( SELECT oficina.id idOficina, oficina.nombre nombreOfi, oficina.zona, Toficina_Equipos.departamento, Toficina_Equipos.idEquipo FROM Toficina_Equipos, oficina WHERE Toficina_Equipos.idOficina = oficina.id ) ofi right JOIN (select * from equipo where tipo='CPU' or tipo='PORTATIL') equiposPC ON ofi.idEquipo = equiposPC.idEquipo) equiposoficina left join (select tequipos_software.idEquipo,tusuarios.usuario,software.nombre nombreSoftware,software.version from tequipos_software, tusuarios, software where tequipos_software.idUsuario=tusuarios.id and tequipos_software.idSoftware=software.id and software.tipo='S.O.') usuarioSoftware on equiposoficina.idEquipo=usuarioSoftware.idEquipo

select * from (select * from (SELECT equiposPC.*, IfNull(ofi.idOficina,'') idOficina, IfNull(ofi.nombreOfi,'') nombreOfi, IfNull(ofi.departamento,'') departamento,IfNull(ofi.zona,'') zona from ( SELECT oficina.id idOficina, oficina.nombre nombreOfi, oficina.zona, Toficina_Equipos.departamento, Toficina_Equipos.idEquipo FROM Toficina_Equipos, oficina WHERE Toficina_Equipos.idOficina = oficina.id ) ofi right JOIN (select * from equipo where tipo='CPU' or tipo='PORTATIL') equiposPC ON ofi.idEquipo = equiposPC.idEquipo) equiposoficina left join (select tequipos_software.idEquipo idEquipoSoft,tusuarios.usuario,software.nombre nombreSoftware,software.version from tequipos_software, tusuarios, software where tequipos_software.idUsuario=tusuarios.id and tequipos_software.idSoftware=software.id and software.tipo='S.O.') usuarioSoftware on equiposoficina.idEquipo=usuarioSoftware.idEquipoSoft) consulta where idOficina like IFNULL(null,'%')

CREATE TABLE tusuarios_laboratorio (
    id int NOT NULL AUTO_INCREMENT, 
usuario varchar(20) NOT NULL,
password varchar(45),
nombre varchar(20),
ultima_sesion DATETIME default '0000-00-00 00:00:00',
    PRIMARY KEY (id)
);

ALTER TABLE `equipo` ADD  CONSTRAINT `FK_CUSUARIO` FOREIGN KEY (`cusuario`) REFERENCES `tusuarios_lab`(`usuario`) ON DELETE NO ACTION ON UPDATE CASCADE;

UPDATE equipo2 SET fhasta=NOW() where idEquipo=47;
INSERT INTO equipo2 ( codActivo,  tipo,  marca, modelo,  serial,  procesador,ram,  discoduro,  nombre,  mac,  ip,  estado,  observacion, cusuario) VALUES
("1.8.06.01.009.3234.001", "CPU", "GIGA", "", "", "", "", "", "", "", "", "", "","marco");

ALTER TABLE `equipo` DROP PRIMARY KEY, ADD PRIMARY KEY( `idEquipo`, `fhasta`, `fdesde`);

SET GLOBAL fncfhasta = '2999-12-31 00:00:00';

ALTER TABLE `oficina` ADD  CONSTRAINT `FK_CUSUARIO_OFICINA` FOREIGN KEY (`cusuario`) REFERENCES `tusuarios_lab`(`usuario`) ON DELETE RESTRICT ON UPDATE CASCADE;

select equipoUsuario.idSoftware, equipoUsuario.licencia,equipoUsuario.idUsuario, equipoUsuario.usuario, concat(equipoUsuario.nombre,' ',equipoUsuario.apellido) nombreUsuario, software.tipo, software.nombre, software.version from (select * from(select tequipos_software.idSoftware,tequipos_software.idUsuario,tequipos_software.licencia from tequipos_software where tequipos_software.fhasta='2999-12-31 00:00:00' and tequipos_software.idEquipo=46) equipo left join tusuarios_pc on tusuarios_pc.id=equipo.idUsuario and tusuarios_pc.fhasta ='2999-12-31 00:00:00') equipoUsuario, software where equipoUsuario.idSoftware=software.id and software.fhasta ='2999-12-31 00:00:00'

INSERT INTO `equipo` (`codActivo`, `tipo`, `marca`, `modelo`, `serial`, `procesador`, `ram`, `discoduro`, `nombre`, `mac`, `ip`, `estado`, `observacion`,`cusuario`) VALUES 
('1.8.06.01.009.3234.001','CPU','ASUS','','','Intel(R) Core(TM) i4',4,500,'','','','','','admin'),
('1.8.06.01.009.3234.001','CPU','GIGA','','','Intel(R) Core(TM) i4',4,500,'','','','','','admin'),
('1.8.06.01.009.3234.001','PORTATIL','LENOVO','','','Intel(R) Core(TM) i3',4,500,'','','','','','admin'),
('1.8.06.01.009.3234.001','CPU','GIGA','','','Intel(R) Core(TM) i5',4,500,'','','','','','admin'),
('1.8.06.01.009.3234.001','PORTATIL','DELL','','','Intel(R) Core(TM) i4',4,500,'','','','','','admin'),
('1.8.06.01.009.3234.001','CPU','GIGA','','','Intel(R) Core(TM) i7',4,500,'','','','','','admin'),
('1.8.06.01.009.3234.001','PORTATIL','TOSHIBA','','','Intel(R) Core(TM) i3',4,500,'','','','','','m.gutama'),
('1.8.06.01.009.3234.001','CPU','GIGA','','','Intel(R) Core(TM) i3',4,500,'','','','','','m.gutama'),
('1.8.06.01.009.3234.001','CPU','ASUS','','','Intel(R) Core(TM) i5',4,500,'','','','','','m.gutama'),
('1.8.06.01.009.3234.001','PORTATIL','ASUS','','','Intel(R) Core(TM) i5',4,500,'','','','','','m.gutama'),
('1.8.06.01.009.3234.001','CPU','GIGA','','','Intel(R) Core(TM) i5',4,500,'','','','','','m.gutama');



INSERT INTO `equipo` (`idEquipo`, `codActivo`, `tipo`, `marca`, `modelo`, `serial`, `procesador`, `ram`, `discoduro`, `nombre`, `mac`, `ip`, `estado`, `observacion`, `fhasta`, `fdesde`, `cusuario`) VALUES (NULL, '2', 'CPU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2999-12-31 00:00:00.000000', current_timestamp(), 'admin');

CREATE TABLE tdepartamentos (
	id int NOT NULL AUTO_INCREMENT, 
	nombre varchar(20) NOT NULL,
	abreviatura varchar(3),
	`fhasta` datetime NOT NULL DEFAULT '2999-12-31 00:00:00',
	`fdesde` datetime NOT NULL DEFAULT current_timestamp(),
	`cusuario` varchar(20) NOT NULL,
    PRIMARY KEY (id, nombre)
);

DROP TRIGGER IF EXISTS toficina_equipos_insert;

CREATE TRIGGER toficina_equipos_insert BEFORE INSERT ON toficina_equipos FOR EACH ROW
	UPDATE toficina_equipos SET fhasta=NOW() where idEquipo=NEW.idEquipo and idOficina=NEW.idOficina and fhasta='2999-12-31 00:00:00';
	
INSERT INTO `toficina_equipos` (`idEquipo`, `idOficina`, `departamento`,cusuario) VALUES (61, 2, 'COORDINACIÓN','admin');

ALTER TABLE penguins AUTO_INCREMENT=1;

CREATE TABLE tzonas (
	id int NOT NULL AUTO_INCREMENT, 
	nombre varchar(20) NOT NULL,
	`fhasta` datetime NOT NULL DEFAULT '2999-12-31 00:00:00',
	`fdesde` datetime NOT NULL DEFAULT current_timestamp(),
	`cusuario` varchar(20) NOT NULL,
    PRIMARY KEY (id, nombre)
);

DROP TABLE IF EXISTS `tzonas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tzonas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(31) NOT NULL,
  `fhasta` datetime NOT NULL DEFAULT '2999-12-31 00:00:00',
  `fdesde` datetime NOT NULL DEFAULT current_timestamp(),
  `cusuario` varchar(20) NOT NULL,
  PRIMARY KEY (`id`,`fhasta`,`fdesde`),
  KEY `FK_CUSUARIO_ZONA` (`cusuario`),
  CONSTRAINT `FK_CUSUARIO_ZONA` FOREIGN KEY (`cusuario`) REFERENCES `tusuarios_lab` (`usuario`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

INSERT INTO `tdepartamentos` VALUES (1,'ADMINISTRACIÓN',NULL,'2999-12-31 00:00:00','2020-12-01 12:29:42','admin'),(2,'ANALISIS DE DATOS',NULL,'2999-12-31 00:00:00','2020-12-01 12:29:42','admin')